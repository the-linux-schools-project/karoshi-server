#!/bin/bash
#Copyright (C) 2009  Paul Sharrad
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"OCS"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_tech

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"	

#Check to see if OCS is enabled
OCSENABLED=no
[ -d /var/www/html_karoshi/tech/ocs ] && OCSENABLED=yes


if [ $OCSENABLED = no ]
then
	echo '<br>'$"The OCS module has not been installed. You can install it in the modules section."''
fi

if [ $OCSENABLED = yes ]
then
	echo '
	<script>
		alert("'"$MESSAGE"'");
		window.location = "/tech/ocs/"
	</script>'
fi

echo '
</display-karoshicontent>
</body>
</html>'
exit
