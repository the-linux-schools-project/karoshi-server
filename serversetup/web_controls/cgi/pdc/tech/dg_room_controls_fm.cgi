#!/bin/bash
#Copyright (C) 2012 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Client Internet Controls"
TITLEHELP=$"Choose the location that you want to allow or deny internet access for."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Room_Controls"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_tech

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"	

#########################
#Get data input
#########################

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/view_karoshi_web_admin_log.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi

#Generate navigation bar
if [ "$MOBILE" = no ]
then
	DIV_ID=actionbox3
	TABLECLASS=standard
	WIDTH=100
	ICON1=/images/assets/locationm.png
	ICON3=/images/submenus/system/clock.png
	ICON4=/images/assets/curriculum_computer.png
else
	DIV_ID=actionbox2
	TABLECLASS=mobilestandard
	WIDTH=110
	ICON1=/images/assets/locationm.png
	ICON3=/images/submenus/system/clockm.png
	ICON4=/images/assets/curriculum_computerm.png
fi

if [ -f /var/lib/samba/netlogon/locations.txt ]
then
	LOCATION_COUNT=$(wc -l < /var/lib/samba/netlogon/locations.txt)
else
	LOCATION_COUNT=0
fi

if [ "$LOCATION_COUNT" != 0 ]
then
	if [ -d /opt/karoshi/asset_register/locations/ ]
	then
		if [[ $(ls -1 /opt/karoshi/asset_register/locations/ | wc -l) -gt 0 ]]
		then
			ROWCOUNT=6
			[ "$MOBILE" = yes ] && ROWCOUNT=3

			echo '
				<form action="dg_room_controls.cgi" method="post">
					<table>
						<tbody>
							<tr>'
			LOCCOUNTER=1

			for LOCATIONS in /opt/karoshi/asset_register/locations/*
			do
				LOCATION=$(basename "$LOCATIONS")
				echo '
								<td style="width: '"$WIDTH"'px; vertical-align:middle;">
									<button class="info" name="_ChooseLocation_" value="_LOCATION_'"$LOCATION"'_">
										<img src="'$ICON1'" alt="'$"Choose Location"'">
										<span>'$"Client Internet Controls"'<br>'"$LOCATION"'</span>
									</button>
								</td>
								<td>'"$LOCATION"'</td>'
				[ "$LOCCOUNTER" = "$ROWCOUNT" ] && echo '
							</tr><tr>'
				let LOCCOUNTER="$LOCCOUNTER"+1
				[ "$LOCCOUNTER" -gt "$ROWCOUNT" ] && LOCCOUNTER=1
			done

			while [ "$LOCCOUNTER" -le "$ROWCOUNT" ]
			do
				echo '
								<td></td>'
				let LOCCOUNTER="$LOCCOUNTER"+1
			done

			echo "
							</tr>
						</tbody>
					</table>
				</form><br>"
		fi
	else
		echo $"No locations have been added."
	fi
else
	echo $"No locations have been created."
fi

echo '
</display-karoshicontent>
</body>
</html>'
exit
