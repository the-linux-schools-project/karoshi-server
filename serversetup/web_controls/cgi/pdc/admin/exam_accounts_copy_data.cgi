#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Exam Accounts - Copy Data"
TITLEHELP=$"Select the exam accounts that you want to copy the data to."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts#Copy_Data_to_Exam_Accounts"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-+')
#########################
#Assign data to variables
#########################
END_POINT=11
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign EXAMSTART
DATANAME=EXAMSTART
get_data
EXAMSTART=$(echo "$DATAENTRY" | tr -cd '0-9')

#Assign EXAMEND
DATANAME=EXAMEND
get_data
EXAMEND=$(echo "$DATAENTRY" | tr -cd '0-9')

#Assign READONLY
DATANAME=READONLY
get_data
READONLY="$DATAENTRY"

#Assign EXCEPTIONLIST
DATANAME=EXCEPTIONLIST
get_data
EXCEPTIONLIST="$DATAENTRY"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/exam_accounts.cgi"
</script>
<display-karoshicontent>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER": /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that all fields are not blank

if [ -z "$EXAMSTART" ] || [ -z "$EXAMEND" ]
then

	MESSAGE=$"You have to enter in a range to copy data to the exam accounts."
	show_status
fi

#Check that numbers are within range of exam accounts

if [ "$EXAMSTART" -gt "$EXAMEND" ] || [ "$EXAMSTART" -le 0 ]
then
	MESSAGE=''$"The input is out of range."' '$"Total number of exam accounts"': '"$EXAMACCOUNTS"''
	show_status
fi

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/exam_accounts_copy_data.cgi | cut -d' ' -f1)
#Copy data to exam accounts

########################
#Check to see if any tar.gz or.zip files have been uploaded and extract them
########################

ZIPFILES=( $(ls /var/www/karoshi/exam_upload/*.zip 2>/dev/null) )

cd /var/www/karoshi/exam_upload

ZIPFILECOUNT="${#ZIPFILES[@]}"
COUNTER=0
while [ "$COUNTER" -lt "$ZIPFILECOUNT" ]
do
	echo '<b>'$"Extracting"' '"${ZIPFILES[$COUNTER]}"'</b><br><br>'

	[ -f "${ZIPFILES[$COUNTER]}" ] && unzip "${ZIPFILES[$COUNTER]}" 1>/dev/null
	[ -f "${ZIPFILES[$COUNTER]}" ] && rm -f "${ZIPFILES[$COUNTER]}"
	let COUNTER="$COUNTER"+1 
done

TARFILES=( $(ls /var/www/karoshi/exam_upload/*.tar.gz 2>/dev/null) )

TARFILECOUNT="${#TARFILES[@]}"

COUNTER=0
while [ "$COUNTER" -lt "$TARFILECOUNT" ]
do
	echo '<b>'$"Extracting"' '"${TARFILES[$COUNTER]}"'</b><br><br>'
	cd /var/www/karoshi/exam_upload 
	[ -f "${TARFILES[$COUNTER]}" ] && tar xzf "${TARFILES[$COUNTER]}"
	[ -f "${TARFILES[$COUNTER]}" ] && rm -f "${TARFILES[$COUNTER]}"
	let COUNTER="$COUNTER"+1 
done

echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$EXAMSTART:$EXAMEND:$READONLY:$EXCEPTIONLIST:" | sudo -H /opt/karoshi/web_controls/exec/exam_accounts_copy_data
if [ "$?" = 0 ]
then
	if [ -z "$EXCEPTIONLIST:" ]
	then
		MESSAGE=$"The data has been copied to the following exam accounts"": exam$EXAMSTART - exam$EXAMEND"
	else
		EXCEPTIONLIST=$(echo "$EXCEPTIONLIST" | sed 's/+/ /g')
		MESSAGE=$"The data has been copied to the following exam accounts"": exam$EXAMSTART - exam$EXAMEND\n\n"$"Exceptions"": $EXCEPTIONLIST"
	fi
else
	MESSAGE=$"The data was not copied."
fi
show_status
exit
