#!/bin/bash
#Copyright (C) 2007 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Manage Print Queues"
TITLEHELP=$"Click on the icons to control the printers in each queue."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Manage_Print_Queues"

#Detect mobile browser
MOBILE=no
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js1">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter({
	headers: {
	3: { sorter: false},
	4: { sorter: false}
    		}
		});
    }
);
</script>
<script id="js2">
$(document).ready(function() 
    { 
        $("#myTable2").tablesorter(); 
    } 
);
</script>
<script id="js3">
$(document).ready(function() 
    { 
        $("#myTable2").tablesorter(); 
    } 
);
</script>
'

#Check that a print server has been declared
function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/printers_fm.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#Append Karoshi content to the #karoshicontent container.
echo "
<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-' | sed 's/____/QUADUNDERSCORE/g' | sed 's/_/12345UNDERSCORE12345/g' | sed 's/QUADUNDERSCORE/_/g')
#########################
#Assign data to variables
#########################
END_POINT=5
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign PRINTER
DATANAME=PRINTER
get_data
PRINTER="${DATAENTRY//12345UNDERSCORE12345/_}"

#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

#Check that a print server has been declared
function show_status {
echo '<SCRIPT language="Javascript">'
echo 'alert("'$"A print server has not yet been set up."'")';
echo 'window.location = "karoshi_servers_view.cgi";'
echo '</script>'
echo "</display-karoshicontent></body></html>"
exit
}

[ ! -f /opt/karoshi/server_network/printserver ] && show_status

PRINTSERVER=$(sed -n 1,1p /opt/karoshi/server_network/printserver)

	echo '
			<form action="/cgi-bin/admin/printers_add_fm.cgi" name="AddPrinters" method="post">
				<button class="button" name="_AddPrinter_" value="_">
					'$"Add Printer"'
				</button>

				<button formaction="/cgi-bin/admin/printers_delete.cgi" class="button" name="_DeletePrinter_" value="_">
					'$"Delete Printer"'
				</button>

				<button formaction="/cgi-bin/admin/printers_view_assigned_fm.cgi" class="button" name="_AssignedPrinter_" value="_">
					'$"Assigned Printers"'
				</button>

				<button formaction="/cgi-bin/admin/file_manager.cgi" class="button" name="_EditPPD_" value="_SERVERTYPE_network_ACTION_ENTER_SERVERNAME_'"$PRINTSERVER"'_LOCATION_/etc/cups/ppd_">
					'$"Edit PPD"'
				</button>

				<button formaction="printer_driver_gen.cgi" class="button" name="_WindowsDriverGen_" value="_">
					'$"Windows Drivers"'
				</button>

				<button formaction="printers_airprint.cgi" class="button" name="_AirPrint_" value="_">
					'$"AirPrint"'
				</button>'


if [ -f /opt/karoshi/server_network/servers/"$PRINTSERVER"/savapage ]
then
	echo '
				<button formaction="http://savapage:8631/admin" class="button" name="_Savapage_" value="_">
					Savapage
				</button>'
fi

echo '
			<button formaction="/cgi-bin/admin/locations.cgi" class="button" name="_ViewLocations_" value="_">
				'$"Locations"'
			</button>'


if [ ! -z "$PRINTER" ]
then
	echo '
			<button formaction="/cgi-bin/admin/printers.cgi" class="button" name="_ShowPrinters_" value="_">
				'$"Show Printers"'
			</button>'
fi

echo '</form>'

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/printers.cgi | cut -d' ' -f1)
sudo -H /opt/karoshi/web_controls/exec/printers "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$MOBILE:$PRINTER:"
echo '</display-karoshicontent></body></html>'
exit
