#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

#Detect mobile browser

source /opt/karoshi/web_controls/detect_mobile_browser

TITLE=$"Enable - Disable Network Backup"
TITLEHELP=$"Choose the server that you want to enable or disable the backup server for."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Enable/Disable_Backup"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'
#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#Check if any backup servers have been enabled.
if [ ! -d /opt/karoshi/server_network/backup_servers/backup_settings/ ]
then
	echo $"No karoshi backup servers have been enabled."
	echo '</display-karoshicontent></body></html>'
	exit
fi

echo '<form action="/cgi-bin/admin/backup_enable_disable.cgi" name="selectservers" method="post">'

if [ $(ls -1 /opt/karoshi/server_network/backup_servers/backup_settings/ | wc -l) = 0 ]
then
	echo $"No karoshi backup servers have been enabled."
	echo '</div></div></body></html>'
	exit
fi


#Get backup status for this server
BACKUPSTATUS="Disable Backup"
BACKUP_ICON=/images/submenus/system/backup_enabled.png

echo '
<table id="myTable" class="tablesorter">
	<thead>
		<tr>
			<th class="karoshi-input"><b>'$"Server"'</b></th>
			<th><b>'$"Backup Server"'</b></th>
			<th><b>'$"Status"'</b></th>
		</tr>
	</thead>
	<tbody>'

for KAROSHI_SERVERS in /opt/karoshi/server_network/backup_servers/backup_settings/*
do
	KAROSHI_SERVER=$(basename "$KAROSHI_SERVERS")
	BACKUPSERVER=$(sed -n 1,1p /opt/karoshi/server_network/backup_servers/backup_settings/"$KAROSHI_SERVER"/backupserver)
	BACKUPSTATUS=$"Disable Backup"
	BACKUP_ICON=/images/submenus/system/backup_enabled.png
	if [ -f /opt/karoshi/server_network/backup_servers/backup_settings/"$KAROSHI_SERVER"/stop_backup ]
	then
		BACKUPSTATUS=$"Enable Backup"
		BACKUP_ICON=/images/submenus/system/backup_disabled.png
	fi
	echo '
		<tr>
			<td>'"$KAROSHI_SERVER"'</td>
			<td>'"$BACKUPSERVER"'</td>
			<td>
				<button class="info" name="_'"$KAROSHI_SERVER"'_" value="_'"$KAROSHI_SERVER"'_">
					<img src="'"$BACKUP_ICON"'" alt="'"$BACKUPSTATUS"'">
					<span>'"$BACKUPSTATUS"'</span>
				</button>
			</td>
		</tr>'

done
echo '
	</tbody>
</table>
</form>
</display-karoshicontent>
</body>
</html>'
exit
