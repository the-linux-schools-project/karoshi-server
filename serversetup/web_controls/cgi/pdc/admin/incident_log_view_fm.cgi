#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"View Incident Logs"
TITLEHELP=$"View the incident logs for a user."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Record_Incident"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
	<form action="/cgi-bin/admin/incident_log_view.cgi" name="selectedsites" method="post">
		<button class="button" formaction="incident_log_add.cgi" name="_ViewIncidentLogs_" value="_">
			'$"Add"'
		</button>
		<br><br>
		<table>
			<tbody>
				<tr>
					<td class="karoshi-input">'$"Filter"'</td>
					<td>
						<select name="_ALPHABET_" class="karoshi-input">
							<option>A</option>
							<option>B</option>
							<option>C</option>
							<option>D</option>
							<option>E</option>
							<option>F</option>
							<option>G</option>
							<option>H</option>
							<option>I</option>
							<option>K</option>
							<option>K</option>
							<option>L</option>
							<option>M</option>
							<option>N</option>
							<option>O</option>
							<option>P</option>
							<option>Q</option>
							<option>R</option>
							<option>S</option>
							<option>T</option>
							<option>U</option>
							<option>V</option>
							<option>W</option>
							<option>X</option>
							<option>Y</option>
							<option>Z</option>
							<option selected="selected" value="ALL">'$"All"'</option>
						</select>
					</td>
				</tr>
			</tbody>
		</table>
	<input value="'$"Submit"'" class="button primary" type="submit">
	</form>
	</display-karoshicontent>
</body>
</html>
'
exit
