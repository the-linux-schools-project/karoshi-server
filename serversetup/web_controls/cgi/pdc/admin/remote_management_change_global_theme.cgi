#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

#Detect mobile browser
MOBILE=no
source /opt/karoshi/web_controls/detect_mobile_browser

TITLE=$"Change Global Theme"
TITLEHELP=$"This will affect all general web management pages."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Web_Management_Themes"

#Get current stylesheet.
CurrentStyleSheet=$(grep STYLESHEET /opt/karoshi/web_controls/global_prefs | cut -d"=" -f2 | cut -d. -f1)
TITLE="$TITLE $CurrentStyleSheet"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<form action="/cgi-bin/admin/remote_management_change_global_theme2.cgi" method="post">

	<button formaction="/cgi-bin/admin/remote_management_change_theme.cgi" class="button" name="_ChangeTheme_" value="_">
		'$"Change Theme"'
	</button>

	<br>
	<br>

	<table>
		<tbody>
			<tr>'

StyleCount=1

if [ "$MOBILE" = yes ]
then
	MaxStyles=2
else
	MaxStyles=3
fi

for Theme in $(ls -1 /var/www/html_karoshi/responsive/assets/css/*.css | cut -d"/" -f8 | sed 's/.css//g' | sed 's/defaultstyle//g' | sed 's/main//g' | sed 's/bluegrey//g' | sed 's/greyblue//g' | sed 's/bronze//g' | sed 's/default//g' | sed 's/font-awesome.min//g' | sed '/^$/d')
do

	StyleSheetPreview=$(basename "$Theme")
	if [ "$StyleSheetPreview" != "$CurrentStyleSheet" ]
	then
		echo '
			<td>
				<button class="button" name="_SetTheme_" value="_THEMECHOICE_'"$StyleSheetPreview"'_">
					'"$StyleSheetPreview"'
				</button>
			</td>'
		let StyleCount="$StyleCount"+1
	fi
	if [ "$StyleCount" -gt "$MaxStyles" ]
	then
		echo '
		</tr><tr>'
		StyleCount=1
	fi
done


[ "$StyleCount" = 1 ] && echo "
				<td></td>"
echo '
			</tr>
		</tbody>
	</table>
	<br>'

echo '
</form>
</display-karoshicontent>
</body>
</html>'
exit

