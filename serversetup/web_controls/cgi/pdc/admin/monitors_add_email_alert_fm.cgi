#!/bin/bash
#Copyright (C) 2010  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"View E-Mail - SMS Alerts"
TITLEHELP=$"This allows you to send e-mail alerts in the event of a network failure."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_eMail_Alerts"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\.%_:\-' | sed 's/%40/@/g')
#########################
#Assign data to variables
#########################
END_POINT=15
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign NAME
DATANAME=NAME
get_data
NAME="$DATAENTRY"

#Assign EMAILTO
DATANAME=EMAILTO
get_data
EMAILTO="$DATAENTRY"

#Assign EMAILFROM
DATANAME=EMAILFROM
get_data
EMAILFROM="$DATAENTRY"

#Assign MAILSERVER
DATANAME=MAILSERVER
get_data
MAILSERVER="$DATAENTRY"


#Check to see if a monitoring server has been setup
if [ -f /opt/karoshi/server_network/monitoringserver ]
then
	echo '
	<form action="monitors_view_email_alerts_fm.cgi" method="post">
		<button class="button" name="_ViewAlerts_" value="_">
			'$"View"'
		</button>

		<button formaction="mon_status.cgi" class="button" name="_MonStatus_" value="_">
			'$"Network Status"'
		</button>
	</form>
	<form action="/cgi-bin/admin/monitors_add_email_alert.cgi" method="post">
  		<table>
			<tbody>
				<tr>
 					<td class="karoshi-input">'$"Contact Name"'</td>
        				<td><input class="karoshi-input" required="required" tabindex= "1" name="_NAME_" value="'"$NAME"'" size="20" type="text"></td>
					<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Monitor_Server#Adding_E-Mail_-_SMS_Alerts"><span class="icon-large-tooltip">'$"Enter a short name for this E-Mail alert."'</span></a></td>
				</tr>
				<tr>
					<td>'$"Send E-Mail to"'</td>
					<td><input class="karoshi-input" required="required" tabindex= "2" name="_EMAILTO_" value="'"$EMAILTO"'" size="20" type="text"></td>
					<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Monitor_Server#Adding_E-Mail_-_SMS_Alerts"><span class="icon-large-tooltip">'$"Enter in the email address you want the alert sent to."'</span></a></td>
				</tr>
				<tr>
					<td>'$"E-Mail from"'</td>
					<td><input class="karoshi-input" required="required" tabindex= "3" name="_EMAILFROM_" value="'"$EMAILFROM"'" size="20" type="text"></td>
					<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Monitor_Server#Adding_E-Mail_-_SMS_Alerts"><span class="icon-large-tooltip">'$"Enter in the email address of the sender."'</span></a></td>
				</tr>
				<tr>
					<td>'$"E-Mail Server"'</td>
					<td><input class="karoshi-input" required="required" tabindex= "4" name="_MAILSERVER_" value="'"$MAILSERVER"'" size="20" type="text"></td>
					<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Monitor_Server#Adding_E-Mail_-_SMS_Alerts"><span class="icon-large-tooltip">'$"Enter in the address of the mail server that you want to send the email to."'</span></a></td>
				</tr>
		</tbody>
	</table>
	<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
	</form>'
else
	echo ''$"A monitoring server has not been setup."''
fi
echo '
</display-karoshicontent>
</body>
</html>
'
exit
