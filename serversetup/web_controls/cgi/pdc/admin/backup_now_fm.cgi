#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

source /opt/karoshi/server_network/security/password_settings

TITLE=$"Run Network Backup Now"
TITLEHELP=$"Choose the server that you want to backup."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Run_Network_Backup_Now"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '
<script>
<!--
function SetAllCheckBoxes(FormName, FieldName, CheckValue)
{
	if(!document.forms[FormName])
		return;
	var objCheckBoxes = document.forms[FormName].elements[FieldName];
	if(!objCheckBoxes)
		return;
	var countCheckBoxes = objCheckBoxes.length;
	if(!countCheckBoxes)
		objCheckBoxes.checked = CheckValue;
	else
		// set the check value for all check boxes
		for(var i = 0; i < countCheckBoxes; i++)
			objCheckBoxes[i].checked = CheckValue;
}
// -->
</script>
<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/karoshi_servers_view.cgi"
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#Check to see that a backup server has been configured
if [ ! -d /opt/karoshi/server_network/backup_servers/servers ]
then
	MESSAGE=$"No karoshi backup servers have been enabled for ssh."
	show_status
fi

if [[ $(ls -1 /opt/karoshi/server_network/backup_servers/servers) = 0 ]]
then
	MESSAGE=$"No karoshi backup servers have been enabled for ssh."
	show_status
fi

echo '<form action="/cgi-bin/admin/backup_now.cgi" name="selectservers" method="post">'

#Show list of servers
/opt/karoshi/web_controls/show_servers "$MOBILE" backups $"Run Network Backup"

echo '
</form>
</display-karoshicontent>
</body>
</html>'
exit
