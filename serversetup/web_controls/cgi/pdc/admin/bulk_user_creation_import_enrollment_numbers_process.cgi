#!/bin/bash
#Copyright (C) 2008 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Import Enrollment Numbers or staff codes"
TITLEHELP=$"The CSV format is username, enrollment number or staff code"
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Import_Enrolment_Numbers"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "
		<display-karoshicontent>
		<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

function show_status {

echo '<SCRIPT language="Javascript">'
echo 'alert("'$MESSAGE'")';
echo 'window.location = "/cgi-bin/admin/bulk_user_creation_import_enrollment_numbers_fm.cgi"'
echo '</script>'
echo "</display-karoshicontent></body></html>"
exit
}
#########################
#Check https access
#########################
if [ https_$HTTPS != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER": /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check input file
[ -d /var/www/karoshi/bulk_user_creation_enrollment_numbers ] || mkdir -p /var/www/karoshi/bulk_user_creation_enrollment_numbers
chmod 0700 /var/www/karoshi/
chmod 0700 /var/www/karoshi/bulk_user_creation_enrollment_numbers
if [ `dir /var/www/karoshi/bulk_user_creation_enrollment_numbers --format=single-column | wc -l` != 1 ]
then
	MESSAGE=$"File upload error."
	show_status
fi
CSVFILE=`ls /var/www/karoshi/bulk_user_creation_enrollment_numbers`
echo >> /var/www/karoshi/bulk_user_creation_enrollment_numbers/"$CSVFILE"
cat /var/www/karoshi/bulk_user_creation_enrollment_numbers/"$CSVFILE" | tr -cd 'A-Za-z0-9\.,_:\-\n' > /var/www/karoshi/bulk_user_creation_enrollment_numbers/"$CSVFILE"2
rm -f /var/www/karoshi/bulk_user_creation_enrollment_numbers/"$CSVFILE"
mv /var/www/karoshi/bulk_user_creation_enrollment_numbers/"$CSVFILE"2 /var/www/karoshi/bulk_user_creation_enrollment_numbers/"$CSVFILE"
sed -i '/^$/d' /var/www/karoshi/bulk_user_creation_enrollment_numbers/"$CSVFILE"
CSVFILE_LINES=`cat /var/www/karoshi/bulk_user_creation_enrollment_numbers/"$CSVFILE" | wc -l`
[ -f /var/www/karoshi/bulk_user_creation_enrollment_numbers/karoshi_enrollmentnumbers.csv ] && rm -f /var/www/karoshi/bulk_user_creation_enrollment_numbers/karoshi_enrollmentnumbers.csv
COUNTER=1
while [ $COUNTER -le $CSVFILE_LINES ]
do
	USERNAME=`sed -n $COUNTER,$COUNTER'p' /var/www/karoshi/bulk_user_creation_enrollment_numbers/"$CSVFILE" | cut -s -d, -f1`
	ENROLLMENT_NO=`sed -n $COUNTER,$COUNTER'p' /var/www/karoshi/bulk_user_creation_enrollment_numbers/"$CSVFILE" | cut -s -d, -f2`
	if [ -z "$USERNAME" ] || [ -z "$ENROLLMENT_NO" ]
	then
		echo Error on line $COUNTER'<br>'
		MESSAGE=`echo $"The CSV file you have chosen is not formatted correctly."`
		show_status
	fi
	echo "$USERNAME","$ENROLLMENT_NO" >> /var/www/karoshi/bulk_user_creation_enrollment_numbers/karoshi_enrollmentnumbers.csv
	let COUNTER=$COUNTER+1
done
CSVMDSUM=`sha256sum /var/www/karoshi/bulk_user_creation_enrollment_numbers/karoshi_enrollmentnumbers.csv | cut -d' ' -f1`
rm -f /var/www/karoshi/bulk_user_creation_enrollment_numbers/"$CSVFILE"
Checksum=`sha256sum /var/www/cgi-bin_karoshi/admin/bulk_user_creation_import_enrollment_numbers_process.cgi | cut -d' ' -f1`
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$CSVMDSUM:" | sudo -H /opt/karoshi/web_controls/exec/bulk_user_creation_import_enrollment_numbers

echo "</display-karoshicontent></body></html>"
