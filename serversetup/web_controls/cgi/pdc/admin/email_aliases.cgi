#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"E-Mail Aliases"
TITLEHELP=$"Control E-Mail Aliases for your users."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=E-Mail_Aliases"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
END_POINT=10
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

#Assign ALIAS
DATANAME=ALIAS
get_data
ALIAS="$DATAENTRY"

#Assign USERNAME
DATANAME=USERNAME
get_data
USERNAME="$DATAENTRY"

#Assign DOMAIN
DATANAME=DOMAIN
get_data
DOMAIN="$DATAENTRY"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/email_aliases.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"The action cannot be blank."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
[ -z "$ACTION" ] && ACTION=view
if [ "$ACTION" = delete ] || [ "$ACTION" = reallyadd ]
then
	if [ -z "$ALIAS" ]
	then
		MESSAGE=$"You have not entered an alias."
		show_status
	fi
	if [ -z "$USERNAME" ]
	then
		MESSAGE=$"You have not entered a username."
		show_status
	fi
	#Check that the username exists
	getent passwd "$USERNAME" 1>/dev/null
	if [ "$?" != 0 ]
	then
		MESSAGE=$"The username does not exist."
		show_status
	fi
	#Check that the domain is not blank
	if [ -z "$DOMAIN" ]
	then
		MESSAGE=$"The domain cannot be blank."
		show_status
	fi
fi

if [ "$ACTION" = add ] || [ "$ACTION" = delete ]
then
	ACTION2=view
	MESSAGE=$"Aliases"
	MESSAGE2=$"View the current aliases."
	HELPMSG=$"Adding an alias will allow emails to be sent to the alias address rather than the actual username."
else
	ACTION2=add
	MESSAGE=$"Add Alias"
	MESSAGE2=$"Add an alias."
	HELPMSG=$"These are the email aliases that are currently active for your email system."
fi


echo '
<form action="/cgi-bin/admin/email_aliases.cgi" method="post">
	<button class="button" name="_DoAction_" value="_ACTION_'$ACTION2'_">
		'$MESSAGE'
	</button>

	<button class="button" formaction="email_domains.cgi" name="_AddDomain_" value="_">
		'$"Domains"'
	</button>
</form>'

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/email_aliases.cgi | cut -d' ' -f1)
#Show aliases
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$ACTION:$ALIAS:$USERNAME:$DOMAIN:$MOBILE:" | sudo -H /opt/karoshi/web_controls/exec/email_aliases
echo '
</display-karoshicontent>
</body>
</html>'
