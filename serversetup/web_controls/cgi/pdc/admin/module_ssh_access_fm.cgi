#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Remote SSH Access"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:%\-+')
#########################
#Assign data to variables
#########################
END_POINT=5
#Assign SERVERNAME

COUNTER=2
while [ "$COUNTER" -le "$END_POINT" ]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [ $(echo "$DATAHEADER"'check') = SERVERNAMEcheck ]
	then
		let COUNTER="$COUNTER"+1
		SERVERNAME=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER="$COUNTER"+1
done

[ ! -z  "$SERVERNAME" ] && TITLE="$TITLE - $SERVERNAME"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/karoshi_servers_view.cgi"
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#########################
#Check data
#########################
#Check to see that servername is not blank
if [ -z "$SERVERNAME" ]
then
	MESSAGE=$"The server cannot be blank."
	show_status
fi

echo '<form name="myform" action="/cgi-bin/admin/module_ssh_access.cgi" method="post">
<input name="_SERVERNAME_" value="'"$SERVERNAME"'" type="hidden">
<b>'$"Description"'</b>
<br>
<br>
'$"This module provides ssh access to the main server."'
<br>
<br>
'$"Consider installing the shell access module instead of this module which gives you a bash shell to the main server embedded into the Web Management."'
<br>

<script>
function showRow(rowId) {
for (var i = 1; i <= "3"; i++) {
	document.getElementById(rowId+i).style.display = "";
    }
}

function hideRow(rowId) {
for (var i = 1; i <= "3"; i++) {
	document.getElementById(rowId+i).style.display = "none";
    }
}

function hideLink(linkId) {
    document.getElementById(linkId).style.display = "none";
}

function showLink(linkId) {
    document.getElementById(linkId).style.display = "";
}
</script>

<button type="button" id="ToggleAdvanced" class="button-no-border icon icon-large fa-chevron-circle-down" onclick="showRow('\''advanced'\''); showLink('\'ToggleBasic\''); hideLink('\'ToggleAdvanced\'');"></button>
<button style="display: none;" type="button" id="ToggleBasic" class="button-no-border icon icon-large fa-chevron-circle-up" onclick="hideRow('\''advanced'\''); showLink('\'ToggleAdvanced\''); hideLink('\'ToggleBasic\'');"></button>
<br>

<p id="advanced1" style="display:none;"><b>'$"Optional Parameters"' - '$"Restrict SSH Access"'</b></p>

<table>
	<tbody>
		<tr id="advanced2" style="display:none;">
			<td class="karoshi-input">'$"Client Address"'</td>
			<td><input class="karoshi-input" tabindex= "2" name="_TCPIP_" size="20" type="text"></td>
			<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Enter in TCPIP numbers or MAC addresses (separated by commas) for machines you want to restrict access to. Leave this blank for unrestricted access."'</span></a></td>
		</tr>
		<tr id="advanced3" style="display:none;">
			<td class="karoshi-input">'$"TCP IP"' / '$"MAC"'</td>
			<td>
				<select class="karoshi-input" name="_RESTRICTTYPE_">
					<option>TCPIP</option>
					<option>MAC</option>
				</select>
			</td>
			<td></td>
		</tr>
	</tbody>
</table>
<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</display-karoshicontent>
</form>
</body>
</html>
'
exit

