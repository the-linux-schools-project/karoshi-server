#!/bin/bash
#Update Server Proxy Settings 
#Copyright (C) 2011  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Configure Server Proxy Settings"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
END_POINT=15
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign SERVERNAME
DATANAME=SERVERNAME
get_data
SERVERNAME="$DATAENTRY"

#Assign SERVERTYPE
DATANAME=SERVERTYPE
get_data
SERVERTYPE="$DATAENTRY"

#Assign SERVERMASTER
DATANAME=SERVERMASTER
get_data
SERVERMASTER="$DATAENTRY"

if [ ! -z "$SERVERNAME" ]
then
	ShortServerName=$(echo "$SERVERNAME" | cut -d. -f1)
	TITLE="$TITLE - $ShortServerName"
fi

#Generate page layout later than normal so that we have caputured the servername for display at the top.
source /opt/karoshi/web_controls/generate_page_admin

function show_status {
echo '
		<script>
			alert("'"$MESSAGE"'");
			window.location = "/cgi-bin/admin/update_server_proxy_settings_choose_server_fm.cgi";
		</script>
	</body>
</html>'
exit
}



#Check to see that a server has been picked to shut down
if [ -z "$SERVERNAME" ]
then
	MESSAGE=$"The servername cannot be blank."
	show_status
fi
#Check to see that a server type is not blank
if [ -z "$SERVERTYPE" ]
then
	MESSAGE=$"The servertype cannot be blank."
	show_status
fi

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"	

echo '
		<form action="/cgi-bin/admin/update_server_proxy_settings.cgi" method="post">'

[ -f /opt/karoshi/server_network/upstream_proxy_settings/"$SERVERNAME" ] && source /opt/karoshi/server_network/upstream_proxy_settings/"$SERVERNAME"


echo '
			<table>
				<tbody>
					<tr>
						<td class="karoshi-input">'$"Proxy name/TCPIP"'</td>
						<td><input tabindex= "1" value="'$TCPIP'" class="karoshi-input" name="_TCPIP_"  size="20" type="text"></td>
						<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Enter in the name or tcpip number of the internet proxy server that you want this server to use for updates."'</span></a></td>
					</tr>
					<tr>
						<td>'$"Proxy Port"'</td>
						<td><input tabindex= "2" value="'$PORT'" class="karoshi-input" name="_PORT_"  size="20" type="text"></td>
						<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Enter in the port number of the internet proxy server."'</span></a></td>
					</tr>
					<tr>
						<td>'$"Username"'</td>
						<td><input tabindex= "3" class="karoshi-input" name="_USERNAME_" value="'$USERNAME'" size="20" type="text"></td>
						<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Enter in a username to connect to the proxy server. Leave this blank if it is not needed."'</span></a></td>
					</tr>
					<tr>
						<td>'$"Password"'</td>
						<td><input tabindex= "4" class="karoshi-input" value="'$PASSWORD'" name="_PASSWORD_" size="20" type="password"></td>
						<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Enter in the new password that you want the user to have."'<br><br>'$"The following special characters are allowed"':<br><br>space ! # $ & ( ) + - =  %</span></a></td>
					</tr>
				</tbody>
			</table>

			<input name="_SERVERNAME_" value="'$SERVERNAME'" type="hidden">
			<input name="_SERVERTYPE_" value="'$SERVERTYPE'" type="hidden">
			<input name="_SERVERMASTER_" value="'$SERVERMASTER'" type="hidden">
			<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
		</form>
		</display-karoshicontent>
	</body>
</html>'
exit
