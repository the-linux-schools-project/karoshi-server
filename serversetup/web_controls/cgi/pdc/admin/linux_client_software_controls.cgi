#!/bin/bash
#Copyright (C) 2007 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Linux Client Software Controls"
TITLEHELP=$"Choose the locations that you want to enable updates and software installs for."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Linux_Client_Software_Controls"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	 window.location = "/cgi-bin/admin/linux_client_software_controls_fm.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

#########################
#Assign data to variables
#########################
END_POINT=6
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign KAROSHIVERSION
DATANAME=KAROSHIVERSION
get_data
KAROSHIVERSION="$DATAENTRY"

if [ ! -z "$KAROSHIVERSION" ]
then
	TITLE="$TITLE - $KAROSHIVERSION"
fi

#########################
#Check data
#########################
#Check to see that VERSION is not blank
if [ -z "$KAROSHIVERSION" ]
then
	MESSAGE=$"The linux version must not be blank."
	show_status
fi

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<form action="/cgi-bin/admin/linux_client_install_software_packages.cgi" name="selectservers" method="post">
	<input name="_KAROSHIVERSION_" value="'"$KAROSHIVERSION"'" type="hidden">
	<button class="button" name="_UPLOAD_" value="_">
		'$"Software Packages"'
	</button>
</form>
<form action="/cgi-bin/admin/linux_client_software_controls2.cgi" name="selectservers" method="post">'

function show_software_status {
#Check current settings
if [ -f "/var/lib/samba/netlogon/linuxclient/$KAROSHIVERSION/software/install/$LOCATION"_install ]
then
	SICON="/images/submenus/client/allowed.png"
	SSTATUS=no
else
	SICON="/images/submenus/client/denied.png"
	SSTATUS=yes
fi
if [ -f "/var/lib/samba/netlogon/linuxclient/$KAROSHIVERSION/software/install/$LOCATION"_updates ]
then
	UICON="/images/submenus/client/allowed.png"
	USTATUS=no
else
	UICON="/images/submenus/client/denied.png"
	USTATUS=yes
fi

echo '
		<tr>
			<td>'"$LOCATION"'</td><td>'$"Enable software install"'</td>
			<td style="text-align:right">
				<button class="info" name="_SoftwareInstall_" value="_SOFTWARE_'"$SSTATUS"'_LOCATION_'"$LOCATION"'_">
					<img src="'"$SICON"'" alt="'$"Install"'">
					<span>'$"This will make the Linux clients install any software in the software install lists."'</span>
				</button>
			</td>
		</tr>
		<tr>
			<td>'"$LOCATION"'</td>
			<td>'$"Enable updates"'</td>
			<td style="text-align:right">
				<button class="info" name="_SoftwareUpdates_" value="_UPDATES_'"$USTATUS"'_LOCATION_'"$LOCATION"'_">
					<img src="'"$UICON"'" alt="'$"Install"'">
					<span>'$"This will make the Linux clients upgrade their software packages."'</span>
				</button>
			</td>
		</tr>'
#echo '<tr><td style="width: 180px;">'$"Auto"'</td><td><a class="info" href="javascript:void(0)"><input name="_AUTO_'$ASTATUS'_" type="image" class="images" src="'$AICON'" value=""><span>'$"Set this to auto to hide the software control dialog from appearing when setting up the clients."'</span></a></td></tr>
#<tr><td style="width: 180px;">'$"Proprietary graphics"'</td><td><a class="info" href="javascript:void(0)"><input name="_GRAPHICS_'$GSTATUS'_" type="image" class="images" src="'$GICON'" value=""><span>'$"This will tell the linux clients to install the relevant proprietary graphics driver for the client."'</span></a></td></tr>
#<tr><td style="width: 180px;">'$"Restricted extras"'</td><td><a class="info" href="javascript:void(0)"><input name="_RESTRICTED_'$RSTATUS'_" type="image" class="images" src="'$RICON'" value=""><span>'$"This will install software that in a few countries licenses may be required."'</span></a></td></tr>
#<tr><td style="width: 180px;">'$"Firmware"'</td><td><a class="info" href="javascript:void(0)"><input name="_FIRMWARE_'$FSTATUS'_" type="image" class="images" src="'$FICON'" value=""><span>'$"This will install proprietary firmware."'</span></a></td></tr>'
}

if [ -f /var/lib/samba/netlogon/linuxclient/"$KAROSHIVERSION"/software/install_on_scheduled_shutdown ]
then
    STARTUP_ICON=/images/submenus/client/denied.png
    SHUTDOWN_ICON=/images/submenus/client/allowed.png
else
    STARTUP_ICON=/images/submenus/client/allowed.png
    SHUTDOWN_ICON=/images/submenus/client/denied.png
fi

#Show controls for auto, graphics drivers and restricted extras
echo '<input name="_KAROSHIVERSION_" value="'"$KAROSHIVERSION"'" type="hidden">
<table class="tablesorter">
	<tbody>
		<tr>
			<td style="width: 413px;">'$"Install software and do updates on startup"'</td>
    			<td>
				<button class="info" name="_SoftwareInstall_" value="_INSTALLTIME_startup_LOCATION_all_">
					<img src="'"$STARTUP_ICON"'" alt="'$"Install"'">
					<span>'$"Install software and do upgrades on startup."'</span>
				</button>
    			</td>
		</tr>
		<tr>
			<td>'$"Install software and do updates on shutdown"'</td>
			<td>
				<button class="info" name="_SoftwareInstall_" value="_INSTALLTIME_shutdown_LOCATION_all_">
					<img src="'"$SHUTDOWN_ICON"'" alt="'$"Install"'">
					<span>'$"Install software and do upgrades on scheduled shutdown."'</span>
				</button>
			</td>
		</tr>
	</tbody>
</table>
<table id="myTable" class="tablesorter" style="text-align: left;" >
	<thead>
		<tr>
			<th class="karoshi-input"><b>'$"Location"'</b></th>
			<th class="karoshi-input">'$"Action"'</th>
			<th></th>
		</tr>
	</thead>
	<tbody>'

LOCATION=all
show_software_status

if [ -f /var/lib/samba/netlogon/locations.txt ]
then
	LOCATION_COUNT=$(wc -l < /var/lib/samba/netlogon/locations.txt)
else
	LOCATION_COUNT=0
fi

COUNTER=1
while [ "$COUNTER" -lt "$LOCATION_COUNT" ]
do
	LOCATION=$(sed -n "$COUNTER,$COUNTER"'p' /var/lib/samba/netlogon/locations.txt)
	show_software_status
	let COUNTER="$COUNTER"+1
done
echo '
	</tbody>
</table>
</form>'

echo '
</display-karoshicontent>
</body>
</html>'
exit

