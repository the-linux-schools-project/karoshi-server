#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Search E-Mail Logs"
TITLEHELP=$"Search the E-Mail logs."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Search_E-Mail_Logs"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"


#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-%+')
#########################
#Assign data to variables
#########################
END_POINT=23

#Assign ACT
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=$(echo $DATA | cut -s -d'_' -f$COUNTER)
	if [ $DATAHEADER'check' = ACTcheck ]
	then
		let COUNTER=$COUNTER+1
		ACT=$(echo $DATA | cut -s -d'_' -f$COUNTER)
		break
	fi
	let COUNTER=$COUNTER+1
done

#Assign DATE
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=$(echo $DATA | cut -s -d'_' -f$COUNTER)
	if [ "$DATAHEADER"'check' = DATEcheck ]
	then
		let COUNTER=$COUNTER+1
		DATE=$(echo $DATA | cut -s -d'_' -f$COUNTER | sed 's/%3A/-/g')
		break
	fi
	let COUNTER=$COUNTER+1
done

#Assign SEARCH1
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=$(echo $DATA | cut -s -d'_' -f$COUNTER)
	if [ "$DATAHEADER"'check' = SEARCH1check ]
	then
		let COUNTER=$COUNTER+1
		SEARCH1=$(echo $DATA | cut -s -d'_' -f$COUNTER | sed 's/%40/@/g')
		break
	fi
	let COUNTER=$COUNTER+1
done

#Assign SEARCH2
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=$(echo $DATA | cut -s -d'_' -f$COUNTER)
	if [ "$DATAHEADER"'check' = SEARCH2check ]
	then
		let COUNTER=$COUNTER+1
		SEARCH2=$(echo $DATA | cut -s -d'_' -f$COUNTER | sed 's/%40/@/g')
		break
	fi
	let COUNTER=$COUNTER+1
done

#Assign SEARCH3
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=$(echo $DATA | cut -s -d'_' -f$COUNTER)
	if [ "$DATAHEADER"'check' = SEARCH3check ]
	then
		let COUNTER=$COUNTER+1
		SEARCH3=$(echo $DATA | cut -s -d'_' -f$COUNTER | sed 's/%40/@/g')
		break
	fi
	let COUNTER=$COUNTER+1
done

#Assign SEARCH4
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=$(echo $DATA | cut -s -d'_' -f$COUNTER)
	if [ "$DATAHEADER"'check' = SEARCH4check ]
	then
		let COUNTER=$COUNTER+1
		SEARCH4=$(echo $DATA | cut -s -d'_' -f$COUNTER | sed 's/%40/@/g')
		break
	fi
	let COUNTER=$COUNTER+1
done

#Assign HOURS
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=$(echo $DATA | cut -s -d'_' -f$COUNTER)
	if [ "$DATAHEADER"'check' = HOURScheck ]
	then
		let COUNTER=$COUNTER+1
		HOURS=$(echo $DATA | cut -s -d'_' -f$COUNTER)
		break
	fi
	let COUNTER=$COUNTER+1
done

#Assign MINUTES
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=$(echo $DATA | cut -s -d'_' -f$COUNTER)
	if [ "$DATAHEADER"'check' = MINUTEScheck ]
	then
		let COUNTER=$COUNTER+1
		MINUTES=$(echo $DATA | cut -s -d'_' -f$COUNTER)
		break
	fi
	let COUNTER=$COUNTER+1
done

if [ -z "$DATE" ]
then
	DATE=$(date +%d-%m-%Y)
fi

DAY=$(echo "$DATE" | cut -d"-" -f1)
MONTH=$(echo "$DATE" | cut -d"-" -f2)
YEAR=$(echo "$DATE" | cut -d"-" -f3)

function show_status {
echo '
<script>
	alert("'$MESSAGE'");
	window.location = "/cgi-bin/admin/email_search_logs_fm.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_$HTTPS != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER": /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

#Show search criteria

echo '
<form name="testform" action="/cgi-bin/admin/email_search_logs.cgi" method="post">
	<input type="hidden" name="_ACT_" value="yes"> 
	<table>
		<tbody>
			<tr>
				<td class="karoshi-input">'$"Date"'</td>
				<td>'

echo "
<!-- calendar attaches to existing form element -->
	<input class=\"karoshi-input\" type=\"text\" value=\"$DAY-$MONTH-$YEAR\" size=14 maxlength=10 name=\"_DATE_\">
	<script type=\"text/javascript\">
	new tcal ({
		// form name
		'formname': 'testform',
		// input name
		'controlname': '_DATE_'
	});

	</script>"

echo '
			</td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Search_E-Mail_Logs"><span class="icon-large-tooltip">'$"Choose the date that you want to search the logs for."'</span></a></td>
		</tr>
		<tr>
			<td>'$"Time"'</td>
			<td>
				<select name="_HOURS_" style="width: 100px; display: inline;">'
if [ ! -z "$HOURS" ]
then
	echo '
					<option>'"$HOURS"'</option>
					<option class="select-dash" disabled="disabled">---</option>'
fi

echo '
					<option label="blank"></option>'

for HOURCHOICE in 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24
do
	echo "
					<option>$HOURCHOICE</option>"
done

echo '
				</select> : <select name="_MINUTES_" style="width: 100px; display: inline;">'

if [ ! -z "$MINUTES" ]
then
	echo '
					<option>'"$MINUTES"'</option>
					<option class="select-dash" disabled="disabled">---</option>'
fi

echo '
					<option label="blank"></option>'

for MINUTECHOICE in 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60
do
	echo "
					<option>$MINUTECHOICE</option>"
done

echo '
				</select>
			</td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Search_E-Mail_Logs"><span class="icon-large-tooltip">'$"Choose the time that you want to search the logs for."' '$"These can be left blank."'</span></a></td>
		</tr>
		<tr>
			<td>'$"Search"' 1</td>
			<td><input class="karoshi-input" tabindex= "3" name="_SEARCH1_" value="'"$SEARCH1"'" size="14" class="karoshi-input" type="text"></td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Search_E-Mail_Logs"><span class="icon-large-tooltip">'$"Enter in the search criteria here."' '$"This could be an email address or message code."'</span></a></td>
		</tr>
		<tr>
			<td>'$"Search"' 2</td>
			<td><input class="karoshi-input" tabindex= "4" name="_SEARCH2_" value="'"$SEARCH2"'" size="14" class="karoshi-input" type="text"></td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Search_E-Mail_Logs"><span class="icon-large-tooltip">'$"Enter in extra search criteria here."'</span></a></td>
		</tr>
		<tr>
			<td>'$"Search"' 3</td>
			<td><input class="karoshi-input" tabindex= "4" name="_SEARCH3_" value="'"$SEARCH3"'" size="14" class="karoshi-input" type="text"></td>
			<td></td>
		</tr>
		<tr>
			<td>'$"Search"' 4</td>
			<td><input class="karoshi-input" tabindex= "4" name="_SEARCH4_" value="'"$SEARCH4"'" size="14" class="karoshi-input" type="text"></td>
			<td></td>
		</tr>
	</tbody>
</table>
<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">'

#Show search results
if [ ! -z "$DATE" ] && [ ! -z "$ACT" ]
then
	Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/email_search_logs.cgi | cut -d' ' -f1)
	echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$DATE:$HOURS:$MINUTES:$SEARCH1:$SEARCH2:$SEARCH3:$SEARCH4:" | sudo -H /opt/karoshi/web_controls/exec/email_search_logs
fi

echo '
</display-karoshicontent>
</form>
</body>
</html>'
exit

