#!/bin/bash
#Copyright (C) 2007 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Linux Client software packages"
TITLEHELP=$"The software shown below will be installed by your linux client computers on boot or shutdown."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Linux_Client_Software_Packages"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/linux_client_install_software_packages_fm.cgi";
</script>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

#########################
#Assign data to variables
#########################
END_POINT=6
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign VERSION
DATANAME=KAROSHIVERSION
get_data
KAROSHIVERSION="$DATAENTRY"

if [ ! -z "$KAROSHIVERSION" ]
then
	TITLE="$TITLE - $KAROSHIVERSION"
fi

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "
		<display-karoshicontent>
		<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Check data
#########################
#Check to see that VERSION is not blank
if [ -z "$KAROSHIVERSION" ]
then
	MESSAGE=$"Blank version"
	show_status
fi



echo '
<form action="/cgi-bin/admin/linux_client_software_controls.cgi" method="post"><input name="_KAROSHIVERSION_" value="'"$KAROSHIVERSION"'" type="hidden">
	<button class="button" name="_UPLOAD_" value="_">
		'$"Software Controls"'
	</button>
</form>'

#Show a table of current software to install and remove
# install - /var/lib/samba/netlogon/linuxclient/version/install_list
# remove - /var/lib/samba/netlogon/linuxclient/version/remove_list

ICON3=/images/submenus/client/delete_software.png

#Show input box
echo '
<form action="/cgi-bin/admin/linux_client_install_software_packages2.cgi" name="selectservers" method="post">
<input name="___KAROSHIVERSION___" value="'"$KAROSHIVERSION"'" type="hidden">
<b>'$"Add software package"'</b><br><br>

	<table>
		<tbody>
			<tr>
				<td class="karoshi-input">'$"Location"'</td>
				<td>'

#Show current rooms
echo '
					<select class="karoshi-input" name="___LOCATION___">
					<option value="all">'$"All locations"'</option>'
if [ -f /var/lib/samba/netlogon/locations.txt ]
then
	for LOCATION in $(cat /var/lib/samba/netlogon/locations.txt)
	do
		echo '
					<option value="'"$LOCATION"'">'"$LOCATION"'</option>'
	done
fi
echo '
					</select>
				</td>
			</tr>
			<tr>
				<td>'$"Software Package"'</td>
				<td><input class="karoshi-input" tabindex= "1" name="___ACTION___add___SOFTWARE___"  size="20" type="text"></td>
			</tr>
		</tbody>
	</table>

	<button class="button" name="___Install___" value="___INSTALL___install___">
		'$"Install"'
	</button>
	 
	<button class="button" name="___Install___" value="___INSTALL___remove___">
		'$"Remove"'
	</button>
</form>
<form action="/cgi-bin/admin/linux_client_install_software_packages2.cgi" name="selectservers" method="post">
<input name="___KAROSHIVERSION___" value="'"$KAROSHIVERSION"'" type="hidden">'

function show_software {
if [ -d /var/lib/samba/netlogon/linuxclient/"$KAROSHIVERSION/software/$INSTALL"/ ]
then
	if [[ $(ls -1 /var/lib/samba/netlogon/linuxclient/"$KAROSHIVERSION/software/$INSTALL"/*_software 2>/dev/null | wc -l) != 0 ]]
	then
		if [ "$SHOW_TABLE_HEADER" = yes ]
		then
			echo '
	<table id="myTable" class="tablesorter" style="text-align: left;" >
		<thead>
			<tr>
				<th class="karoshi-input"><b>'$"Location"'</b></th>
				<th class="karoshi-input"><b>'$"Action"'</b></th>
				<th style="width: '"$WIDTH1"'px;"><b>'$"Software Package"'</b></th>
				<th style="width: '"$WIDTH2"'px;"><b>'$"Delete"'</b></th>
			</tr>
		</thead>
		<tbody>'
			SHOW_TABLE_HEADER=no
			SHOW_TABLE_FOOTER=yes
		fi
		for LOCATIONS in $(ls -1 /var/lib/samba/netlogon/linuxclient/"$KAROSHIVERSION/software/$INSTALL"/*_software)
		do
			#Show install list
			LOCATION=$(basename "$LOCATIONS" | cut -d"_" -f1)
			if [[ $(wc -l < "/var/lib/samba/netlogon/linuxclient/$KAROSHIVERSION/software/$INSTALL/$LOCATION"_software) -gt 0 ]]
			then
				for SOFTWARE in $(cat /var/lib/samba/netlogon/linuxclient/"$KAROSHIVERSION/software/$INSTALL/$LOCATION"_software)
				do
					echo '
			<tr>
				<td>'"$LOCATION"'</td>
				<td>'"$INSTALL_LANG"'</td>
				<td>'"$SOFTWARE"'</td>
				<td>
					<button class="info" name="___Delete___" value="___ACTION___delete___INSTALL___'"$INSTALL"'___SOFTWARE___'"$SOFTWARE"'___LOCATION___'"$LOCATION"'___">
						<img src="'"$ICON3"'" alt="'$"Delete"'">
						<span>'$"Delete" "$SOFTWARE"'</span>
					</button>
				</td>
			</tr>'
				done
			fi
		done
	fi
fi
}

SHOW_TABLE_HEADER=yes
SHOW_TABLE_FOOTER=no
INSTALL=install
INSTALL_LANG=$"Install"
show_software
INSTALL=remove
INSTALL_LANG=$"Remove"
show_software
if [ "$SHOW_TABLE_FOOTER" = yes ]
then
	echo '
		</tbody>
	</table>'
fi

echo '
</form>
</display-karoshicontent>
</body>
</html>'
exit

