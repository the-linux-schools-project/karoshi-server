#!/bin/bash
#Copyright (C) 2013  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

TITLE=$"User Web Folders"
TITLEHELP=$"This allows users to have web folders hosted from their home areas. Any files and folders in a public_html folder in the user's home area will be available via apache on their server."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=User_web_folders"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'
#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
END_POINT=11
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign group
DATANAME=GROUP
get_data
GROUP="$DATAENTRY"

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

#Assign USERNAME
DATANAME=USERNAME
get_data
USERNAME="$DATAENTRY"

#Assign SERVICECHECK
DATANAME=SERVICECHECK
get_data
SERVICECHECK="$DATAENTRY"

[ -z "$ACTION" ] && ACTION=check
[ -z "$SERVICECHECK" ] && SERVICECHECK=yes
[ -z "$USERNAME" ] && USERNAME=notset

if [ ! -z "$GROUP" ]
then
	#Show a button to choose a group
	echo '
	<form name="myform" action="/cgi-bin/admin/user_web_folders.cgi" method="post">
		<button class="button" name="_ChooseGroup_" value="_">
			'$"Select Group"'
		</button>
	</form>
'
else
	#Show list of groups to check
	echo '
	<form name="myform" action="/cgi-bin/admin/user_web_folders.cgi" method="post">
	<table>
		<tbody>
		<tr>
				<td class="karoshi-input">
					'$"Primary Group"'
				</td>
				<td>'
				/opt/karoshi/web_controls/group_dropdown_list
	echo '		</td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=User_web_folders"><span class="icon-large-tooltip">'$"Choose the group that you want to set the web folder status for."'</span></a>
			</td>
		</tr>
	</tbody></table>
	<input value="'$"Submit"'" class="button primary" type="submit">
	</form></display-karoshicontent></body></html>'
	exit
fi
echo '<form name="myform" action="/cgi-bin/admin/user_web_folders.cgi" method="post">'
Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/user_web_folders.cgi | cut -d' ' -f1)
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$GROUP:$ACTION:$USERNAME:$SERVICECHECK:$MOBILE:" | sudo -H /opt/karoshi/web_controls/exec/user_web_folders

echo '</form></display-karoshicontent></body></html>'
exit

