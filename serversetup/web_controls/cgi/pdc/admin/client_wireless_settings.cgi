#!/bin/bash
#Copyright (C) 2013 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Client Wireless Settings"
TITLEHELP=$"This will save the wifi information on the netlogon share for your clients to use when accessing wifi access points."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Client_Wireless_Settings"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-%*+-')
#########################
#Assign data to variables
#########################
END_POINT=9
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign KEY
DATANAME=KEY
get_data
KEY="$DATAENTRY"

#Assign SSID
DATANAME=SSID
get_data
SSID="$DATAENTRY"

function show_status {
echo '<SCRIPT language="Javascript">
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/change_password_fm.cgi"
</script>
</body></html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^$REMOTE_USER: /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/client_wireless_settings.cgi | cut -d' ' -f1)
#########################
#Check data
#########################
#Check to see that the key is not blank
if [ -z "$KEY" ]
then
	MESSAGE=$"No key has been entered."
	show_status
fi

#Check to see that the ssid is not blank
if [ -z "$SSID" ]
then
	MESSAGE=$"No ssid has been entered."
	show_status
fi

echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$KEY:$SSID" | sudo -H /opt/karoshi/web_controls/exec/client_wireless_settings
MESSAGE=$"The Wifi settings have been saved to the netlogon folder."
show_status
exit
