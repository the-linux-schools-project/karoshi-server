#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Clear Web Management Warning Messages"
TITLEHELP=$"This will clear the warning messages that appear at the top of the web management pages."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Clear_Warning_Messages"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

function show_status {
echo '
<script>
	alert("'$MESSAGE'");
	window.location = "/cgi-bin/admin/clear_warnings_fm.cgi";
</script>
</body></html>'
exit
}
#########################
#Check https access
#########################
if [ https_$HTTPS != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [ `grep -c ^$REMOTE_USER: /opt/karoshi/web_controls/web_access_admin` != 1 ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/clear_warnings.cgi | cut -d' ' -f1)
#Clear Warnings
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:" | sudo -H /opt/karoshi/web_controls/exec/clear_warnings
STATUS="$?"
if [ "$STATUS" != 0 ]
then
	[ $STATUS = 101 ] && MESSAGE=''$"There was a problem with this action."' '$"Please check the karoshi web administration logs for more details."''
	[ $STATUS = 102 ] && MESSAGE=$"There are no warning messages to clear."
	show_status
fi
echo '
<script>
	window.location = "/cgi-bin/admin/clear_warnings_fm.cgi";
</script>
<body></html>
'
exit
