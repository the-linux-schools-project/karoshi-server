#!/bin/bash
#Copyright (C) 2007 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Client Boot Controls"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

DISABLESORTCOL=4
[ "$MOBILE" = yes ] && DISABLESORTCOL=2

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter({
	headers: {
	1: { sorter: "MAC" },
	2: { sorter: "ipAddress" },
	'"$DISABLESORTCOL"': { sorter: false },
    		}
		});
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:%\+-')
#########################
#Assign data to variables
#########################
END_POINT=8
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign LOCATION
DATANAME=LOCATION
get_data
LOCATION="$DATAENTRY"

#Assign NETBOOT
DATANAME=NETBOOT
get_data
NETBOOT="$DATAENTRY"


#Assign _SEARCH_
if [ "$LOCATION"'null' = SEARCHNOTVALIDnull ]
then
	END_POINT=8
	DATANAME=SEARCH
	get_data
	SEARCH="$DATAENTRY"
fi

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/client_boot_controls_fm.cgi";
</script>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You have not chosen a location."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################

#Check to see that LOCATION is not blank
if [ -z "$LOCATION" ]
then
	MESSAGE=$"You have not chosen a location."
	show_status
fi

if [ "$LOCATION"'null' = SEARCHNOTVALIDnull ]
then
	#Check to see that SEARCH is not blank
	if [ -z "$SEARCH" ]
	then
		MESSAGE=$"The asset search cannot be blank."
		show_status
	fi
	LOCATION=$SEARCH
fi

echo '
<form action="/cgi-bin/admin/client_boot_controls2.cgi" method="post">
	<button formaction="client_boot_controls_fm.cgi" class="button" name="_ChooseLocation" value="_">
		'$"Choose Location"'
	</button>

	<button class="button" name="_EnableAll_" value="_ACTION_enableall_LOCATION_'"$LOCATION"'_ASSET_none_TCPIP_none_MACADDRESS_none_">
		'$"Enable All"'
	</button>

	<button class="button" name="_ResetAll_" value="_ACTION_resetall_LOCATION_'"$LOCATION"'_ASSET_none_TCPIP_none_MACADDRESS_none_">
		'$"Reset All"'
	</button>

	<button class="button" name="_ActivateChanges_" value="_ACTION_activatechanges_LOCATION_'"$LOCATION"'_ASSET_none_TCPIP_none_MACADDRESS_none_">
		'$"Activate Changes"'
	</button>

	<button class="button" name="_WakeOnLanAll_" value="_ACTION_wakeonlanall_LOCATION_'"$LOCATION"'_ASSET_none_TCPIP_none_MACADDRESS_none_">
		'$"Wake location"'
	</button>

	<button formaction="/cgi-bin/admin/asset_register_view.cgi" class="button" name="_AssetRegister_" value="_ACTION_view_LOCATION_'"$LOCATION"'_">
		'$"Asset Register"'
	</button>
'

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/client_boot_controls.cgi | cut -d' ' -f1)
sudo -H /opt/karoshi/web_controls/exec/client_boot_controls "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$LOCATION:$SEARCH:$MOBILE:$NETBOOT:"

echo '
</form>
</display-karoshicontent>
</body>
</html>'
exit
