#!/bin/bash
#Copyright (C) 2010  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

TITLE=$"Reverse Proxy Sites"
TITLEHELP=$"The following sites are currently being redirected through this proxy server."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Reverse_Proxy_Server#Viewing_and_Deleting_Reverse_Proxy_Entries"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"


WIDTH1=180
WIDTH2=180
WIDTH3=300
WIDTH4=80
TABLECLASS=standard
ICON1=/images/submenus/web/delete.png
if [ "$MOBILE" = yes ]
then
	WIDTH1=180
	WIDTH2=90
	WIDTH3=300
	WIDTH4=80
	TABLECLASS=mobilestandard
	ICON1=/images/submenus/web/deletem.png
fi




function show_status {
echo '<script>'
[ ! -z "$MESSAGE" ] && echo 'alert("'"$MESSAGE"'")'
echo '
	window.location = "reverse_proxy_add_fm.cgi"
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#Check to see if there are any proxy sites
if [ ! -d /opt/karoshi/server_network/reverseproxy/sites/ ]
then
	show_status
fi

if [[ $(ls -1 /opt/karoshi/server_network/reverseproxy/sites/ | wc -l) = 0 ]]
then
	show_status
fi

#Get reverse proxy server
PROXYSERVER=$(sed -n 1,1p /opt/karoshi/server_network/reverseproxyserver | sed 's/ //g')

if [ -z "$PROXYSERVER" ]
then
	MESSAGE=$"A reverse proxy server has not been setup."
	show_status
fi

echo '

<form action="reverse_proxy_add_fm.cgi" method="post">
	<button class="button" name="_AddReverseProxy_" value="_">
		'$"Add"'
	</button>
</form>
<form action="/cgi-bin/admin/reverse_proxy_view.cgi" method="post">
	<table id="myTable" class="tablesorter">
		<thead>
			<tr>'

[ "$MOBILE" = no ] && echo '
				<th style="width: '"$WIDTH2"'px;"><b>'$"Target"'</b></th>'
echo '
				<th style="width: '"$WIDTH3"'px;"><b>'$"Destination"'</b></th>
				<th style="width: '"$WIDTH4"'px;"><b>'$"Delete"'</b></th>
			</tr>
		</thead>
		<tbody>'

for SITES in /opt/karoshi/server_network/reverseproxy/sites/*
do
	SITE=$(basename "$SITES")
	SITE2=$(echo "$SITE" | sed 's/%3A//g' | sed 's/%2F/\//g' | sed 's/\/\///g')
	REDIRECT=$(sed -n 6,6p /opt/karoshi/server_network/reverseproxy/sites/"$SITE" | cut -d' ' -f2- | sed 's/;//g')
	echo '
			<tr>'
	[ "$MOBILE" = no ] && echo '
				<td>'"$SITE2"'</td>'
	echo '
				<td>'"$REDIRECT"'</td>
				<td>
					<button class="info" name="_DoDelete_" value="_ACTION_DELETE_FOLDER_'"$SITE"'_">
						<img src="'"$ICON1"'" alt="'$"Delete"'">
						<span>'$"Delete" "$SITE2"'</span>
					</button>
				</td>
			</tr>'
done

echo '
		</tbody>
	</table>
</form>
</display-karoshicontent>
</body>
</html>'
exit
