#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

#Detect mobile browser
MOBILE=no
source /opt/karoshi/web_controls/detect_mobile_browser

TITLE=$"Computer Logs"
TITLEHELP=$"Internet logs are updated every three minutes."
HELPURL="#"

DATE_INFO=$(date +%F)
DAY=$(echo "$DATE_INFO" | cut -d- -f3)
MONTH=$(echo "$DATE_INFO" | cut -d- -f2)
YEAR=$(echo "$DATE_INFO" | cut -d- -f1)

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '
<script src="/all/calendar2/calendar_eu.js"></script>
        <!-- Timestamp input popup (European Format) -->
<link rel="stylesheet" href="/all/calendar2/calendar.css">
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '<form action="/cgi-bin/admin/dg_view_computer_logs.cgi" name="testform" method="post">

<table>
	<tbody>
		<tr>
			<td class="karoshi-input">'$"Client TCPIP"'</td>
			<td><input required="required" tabindex="1" name="_TCPIP_" type="text" size="14" class="karoshi-input"></td>
			<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Enter in the username that you want to check the internet logs for."'</span></a></td>
		</tr>
		<tr>
			<td>'$"Log Date"'</td>
			<td>'
echo "
				<!-- calendar attaches to existing form element -->
				<input  class=\"karoshi-input\" required=\"required\" tabindex=\"2\" type=\"text\" value=\"$DAY-$MONTH-$YEAR\" size=14 maxlength=10 name=\"_DATE_\">
			</td>
			<td>
				<script>
					new tcal ({
						// form name
						'formname': 'testform',
						// input name
						'controlname': '_DATE_'
					});

				</script>
			</td>
		</tr>"

echo '
		<tr>
			<td>'$"Number of days to view"'</td>
			<td><input  class="karoshi-input" required="required" min="1" max="999" tabindex="3" name="_DAYCOUNT_" maxlength="2" size="2" value="1" type="number"></td>
			<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"This shows the number of sites a user has visited."'</span></a></td>
		</tr>
	</tbody>
	</table>'


echo '<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset"></div>'



echo '
</form>
</display-karoshicontent>
</body>
</html>'
exit
