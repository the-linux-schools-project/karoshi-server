#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Bulk User Actions"
TITLEHELP=$"This will affect a group of users."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Bulk_User_Actions"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-%*+-' | sed 's/*/%1123/g' | sed 's/____/QUADRUPLEUNDERSCORE/g' | sed 's/_/REPLACEUNDERSCORE/g' | sed 's/QUADRUPLEUNDERSCORE/_/g')
#########################
#Assign data to variables
#########################
END_POINT=15
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign OPTIONCHOICE
DATANAME=OPTIONCHOICE
get_data
OPTIONCHOICE="$DATAENTRY"

#Assign NEWPASSWORD
DATANAME=NEWPASSWORD
get_data
NEWPASSWORD="$DATAENTRY"

#Assign GROUP
DATANAME=GROUP
get_data
GROUP="$DATAENTRY"

#Assign NEWGROUP
DATANAME=NEWGROUP
get_data
NEWGROUP="$DATAENTRY"

#Assign MODCODE
DATANAME=MODCODE
get_data
MODCODE="$DATAENTRY"

#Assign FORMCODE
DATANAME=FORMCODE
get_data
FORMCODE="$DATAENTRY"

#Assign EXCEPTIONLIST
DATANAME=EXCEPTIONLIST
get_data
EXCEPTIONLIST="$DATAENTRY"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/modify_groups_fm.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that optionchoice is not blank
if [ -z "$OPTIONCHOICE" ]
then
	MESSAGE=$"The option choice must not be blank."
	show_status
fi
#Check to see that the group is not blank
if [ -z "$GROUP" ]
then
	MESSAGE=$"The group must not be blank."
	show_status
fi

#Check to see that the group exists
getent group "$GROUP" 1>/dev/null
if [ $? != 0 ]
then
	MESSAGE=$"This group does not exist."
	show_status
fi

#Check to see that the option choice is correct
if [ "$OPTIONCHOICE" != enable ] && [ "$OPTIONCHOICE" != disable ] && [ "$OPTIONCHOICE" != changepasswords ] && [ "$OPTIONCHOICE" != resetpasswords ] && [ "$OPTIONCHOICE" != deleteaccounts ] && [ "$OPTIONCHOICE" != deleteaccounts2 ] && [ "$OPTIONCHOICE" != changepassnextlogon ] && [ "$OPTIONCHOICE" != passwordsneverexpire ] && [ "$OPTIONCHOICE" != passwordsexpire ] && [ "$OPTIONCHOICE" != changeprigroup ]
then
	MESSAGE=$"Incorrect option chosen."
	show_status
fi

#Check to see that MODCODE is not blank
if [ -z "$MODCODE" ]
then
	MESSAGE=$"The modify code must not be blank."
	show_status
fi
#Check to see that FORMCODE is not blank
if [ -z "$FORMCODE" ]
then
	MESSAGE=$"The form code must not be blank."
	show_status
fi
#Make sure that FORMCODE and MODCODE matches
if [ "$FORMCODE" != "$MODCODE" ]
then
	MESSAGE=$"Incorrect modify code."
	show_status
fi

#Check to see that passwords have been entered and are correct
if [ "$OPTIONCHOICE" = changepasswords ]
then
	if [ -z "$NEWPASSWORD" ]
	then
		MESSAGE=$"The passwords must not be blank."
		show_status
	fi
fi

if [ "$OPTIONCHOICE" = enable ]
then
	MESSAGE=''$"Enable all users in group"' '"$GROUP"''
fi
if [ "$OPTIONCHOICE" = disable ]
then
	MESSAGE=''$"Disable all users in group"' '"$GROUP"''
fi
if [ "$OPTIONCHOICE" = changepasswords ]
then
	MESSAGE=''$"Change passwords for all users in group"' '"$GROUP"''
fi
if [ "$OPTIONCHOICE" = resetpasswords ]
then
	MESSAGE=''$"Reset passwords all users in group"' '"$GROUP"''
fi
if [ "$OPTIONCHOICE" = deleteaccounts ]
then
	MESSAGE=''$"Delete all users in group"' '"$GROUP"''
fi
if [ "$OPTIONCHOICE" = deleteaccounts2 ]
then
	MESSAGE=''$"Archive and delete all users in group"' '"$GROUP"''
fi
if [ "$OPTIONCHOICE" = changepassnextlogon ]
then
	MESSAGE=''$"Change password on next logon"' '"$GROUP"''
fi

if [ "$OPTIONCHOICE" = passwordsneverexpire ]
then
	MESSAGE=''$"Passwords never expire"' '"$GROUP"''
fi
if [ "$OPTIONCHOICE" = passwordsexpire ]
then
	MESSAGE=''$"Passwords expire"' '"$GROUP"''
fi

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/modify_groups.cgi | cut -d' ' -f1)


echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$OPTIONCHOICE:$GROUP:$NEWPASSWORD:$EXCEPTIONLIST:$NEWGROUP:" | sudo -H /opt/karoshi/web_controls/exec/modify_groups
MODIFY_STATUS="$?"
if [ "$MODIFY_STATUS" = 102 ]
then
	MESSAGE=$"The form code must not be blank."
	show_status
fi

if [ "$OPTIONCHOICE" != resetpasswords ]
then
	MESSAGE=''$"Action completed for"' '"$GROUP"'.'
	show_status
else
	echo '<br><br>'$"Action completed for" "$GROUP"'<br>'
fi
echo '</display-karoshicontent></body></html>'
exit
