#!/bin/bash
#module_fileserver
#Copyright (C) 2010 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:%\-+')

#########################
#Assign data to variables
#########################
END_POINT=3
#Assign SERVERNAME
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = SERVERNAMEcheck ]
	then
		let COUNTER=$COUNTER+1
		SERVERNAME=`echo $DATA | cut -s -d'_' -f$COUNTER`
		break
	fi
	let COUNTER=$COUNTER+1
done

TITLE=$"Setup Internet Proxy Server"' - '"$SERVERNAME"
TITLEHELP=$"This will setup a a squid proxy server for providing access to the internet for the client computers on the network."' '$"Internet filtering is provided by E2Guardian and all internet access through the system is logged."' '$"Logs for all user internet access can be viewed in the web management."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Change_Password"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/karoshi_servers_view.cgi"
	</script>
</display-karoshicontent>
</body>
</html>'
exit
}

function completed {
echo '
<script>
	window.location = "/cgi-bin/admin/karoshi_servers_view.cgi"
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [ $(grep -c ^"$REMOTE_USER": /opt/karoshi/web_controls/web_access_admin) != 1 ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

#########################
#Check data
#########################
if [ -z "$SERVERNAME" ]
then
	MESSAGE=$"You have not chosen a server."
	show_status
fi

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/module_squid.cgi | cut -d' ' -f1)
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$SERVERNAME:" | sudo -H /opt/karoshi/web_controls/exec/module_squid

if [ "$?" = 101 ]
then
	MESSAGE=$"There was a problem with this action." $"Please check the karoshi web administration logs for more details."
	show_status
fi
completed
echo '</display-karoshicontent></body></html>'
exit

