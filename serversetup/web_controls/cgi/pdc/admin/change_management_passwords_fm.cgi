#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

TITLE=$"Management Passwords"
TITLEHELP=$"This is used to change important passwords on your system that are needed to access specific services."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Management_Passwords"
TOOLTIPS=inforight

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '
<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "
	<display-karoshicontent>
	<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
		<form action="/cgi-bin/admin/change_management_passwords.cgi" method="post">
			<table>
				<tbody>
					<tr>
						<td class="karoshi-input">System Password</td>
						<td>
							<select required="required" name="____USERACCOUNT____"  tabindex="1" class="karoshi-input">
								<option label="blank"></option>
								<option>karoshi</option>
								<option>Administrator</option>
								<option>mysql</option>
								<option>root</option>
							</select>
						</td>
						<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Management_Passwords"><span class="icon-large-tooltip"><b>Karoshi</b> - '$"This is the account used to log in locally to the servers."'<br><br><b>administrator</b> - '$"This password is needed to join the clients to the domain."'<br><br><b>Root</b> - '$"You should not normally need to use this password."'</span></a>
</td>
					</tr>
					<tr>
						<td>'$"New Password"'</td><td><input required="required" name="____PASSWORD1____"  tabindex="2" size="20" class="karoshi-input" type="password"></td><td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Management_Passwords"><span class="icon-large-tooltip">'$"Enter in the pasword that you want to use."'<br><br><b>'$"Special Characters"'</b><br><br>'"$CHARACTERHELP"' space !	&quot;	# 	$	%	&amp; 	(	) 	*	+	, 	-	.	/ 	:
;	&lt;	=	&gt;	?	@ 	[	\	]	^	_	` 	{	|	}	~
</span></a></td>
					</tr>
					<tr>
						<td>'$"Confirm Password"'</td><td><input required="required" name="____PASSWORD2____"  tabindex="3" size="20" class="karoshi-input" type="password"></td>
						<td></td>
					</tr>
				</tbody>
			</table>'

#Show list of servers
/opt/karoshi/web_controls/show_servers "$MOBILE" all $"Change password" no no ____


echo '
		</form>
	</display-karoshicontent>
</body>
</html>'
exit

