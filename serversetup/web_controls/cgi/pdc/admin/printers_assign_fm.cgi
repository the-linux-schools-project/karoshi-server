#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Assign Printers to Locations"
TITLEHELP=$"Assign a printer to a location so that it can automatically be assigned to client computers in that location."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_Network_Printer"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "
		<display-karoshicontent>
		<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

function show_status {
echo '<SCRIPT language="Javascript">'
echo 'alert("'"$MESSAGE"'")';
echo '                window.location = "/cgi-bin/admin/printers.cgi";'
echo '</script>'
echo "</display-karoshicontent></body></html>"
exit
}

function no_locations {
echo '<SCRIPT language="Javascript">'
echo 'alert("'"$MESSAGE"'")';
echo '                window.location = "/cgi-bin/admin/locations.cgi";'
echo '</script>'
echo "</display-karoshicontent></body></html>"
exit
}

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:%\-+' | sed 's/____/QUADUNDERSCORE/g' | sed 's/_/12345UNDERSCORE12345/g' | sed 's/QUADUNDERSCORE/_/g')
#########################
#Assign data to variables
#########################
END_POINT=3
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign _PRINTERNAME_
DATANAME=PRINTERNAME
get_data
PRINTERNAME="${DATAENTRY//12345UNDERSCORE12345/_}"

#Check to see that PRINTER is not blank
if [ -z "$PRINTERNAME"	 ]
then
	MESSAGE=$"You have not chosen any printers."
	show_status
fi


echo '
	<form action="/cgi-bin/admin/printers_assign.cgi" method="post">

		<button formaction="/cgi-bin/admin/printers.cgi" class="button" name="_ShowPrinters_" value="_">
			'$"Show Printers"'
		</button>

		<button formaction="/cgi-bin/admin/printers_delete.cgi" class="button" name="_DeletePrinters_" value="_">
			'$"Delete Printer"'
		</button>

		<button formaction="/cgi-bin/admin/printers_view_assigned_fm.cgi" class="button" name="_AssignedPrinter_" value="_">
			'$"Assigned Printers"'
		</button>

		<button formaction="/cgi-bin/admin/locations.cgi" class="button" name="_ViewLocations_" value="_">
			'$"Locations"'
		</button>
		<br>
		<br>'

#Check to see that locations.txt exists
if [ ! -f /var/lib/samba/netlogon/locations.txt ]
then
	MESSAGE=$"No locations have been created."
	no_locations
	exit
fi

###############################
#Location
###############################
if [ -f /var/lib/samba/netlogon/locations.txt ]
then
	LOCATION_COUNT=$(wc -l < /var/lib/samba/netlogon/locations.txt)
else
	LOCATION_COUNT=0
fi

MAXCOLS=2
MAXHEADERS=1
if [ "$LOCATION_COUNT" -gt 10 ]
then
	MAXCOLS=4
	MAXHEADERS=2
fi

if [ "$LOCATION_COUNT" -gt 20 ]
then
	MAXCOLS=6
	MAXHEADERS=3
fi

if [ "$LOCATION_COUNT" -gt 30 ]
then
	MAXCOLS=8
	MAXHEADERS=4
fi

echo '<input type="hidden" name="____PRINTERNAME____" value="'"$PRINTERNAME"'">
<table class="tablesorter" style="text-align: left;" ><tbody><tr><td class="karoshi-input">Printer</td><td style="width: 100px;">'"$PRINTERNAME"'</td></tr>
</tbody></table><br><table class="tablesorter" style="text-align: left;" ><thead><tr><th style="width: 300px; vertical-align: top;"><b>'$"Location"'</b></th><th style="width: 100px; vertical-align: top;"><b>'$"Assign"'</b></th>'


for (( i=1; i<MAXHEADERS; i++ ))
do
	echo '<th style="width: 140px; vertical-align: top;"><b>'$"Location"'</b></th><th style="width: 90px; vertical-align: top;"><b>'$"Assign"'</b></th>'
done

echo '</tr></thead><tbody>'

COUNTER=1
COLCOUNT=0

while [ "$COUNTER" -lt "$LOCATION_COUNT" ]
do
	LOCATION=$(sed -n "$COUNTER,$COUNTER""p" /var/lib/samba/netlogon/locations.txt)

	if [ "$COLCOUNT" = 0 ]
	then
 		 echo "<tr>"
	fi

	if [[ $(grep ^"$LOCATION," /var/lib/samba/netlogon/printers.txt | grep -c -w "$PRINTERNAME") -gt 0 ]]
	then
		echo '<td>'"$LOCATION"'</td><td><input id="'"$LOCATION"'" type="checkbox" name="____LOCATION____" value="'"$LOCATION"'" checked><label for="'"$LOCATION"'"> </label></td>'
	else
		echo '<td>'"$LOCATION"'</td><td><input id="'"$LOCATION"'" type="checkbox" name="____LOCATION____" value="'"$LOCATION"'"><label for="'"$LOCATION"'"> </label></td>'
	fi
	let COUNTER="$COUNTER"+1
	let COLCOUNT="$COLCOUNT"+2

	if [ "$COLCOUNT" = "$MAXCOLS" ]
	then
		echo "</tr>"
		COLCOUNT=0
	fi
done

if [ "$COLCOUNT" -lt "$MAXCOLS" ] && [ "$COLCOUNT" != 0 ]
then
	while [ "$COLCOUNT" -lt "$MAXCOLS" ]
	do
		echo "<td></td>"
		let COLCOUNT="$COLCOUNT"+1
	done
	echo "</tr>"
fi

echo '</tbody></table>'

echo '<input value="'$"Submit"'" class="button primary" type="submit"></form></display-karoshicontent></body></html>'
exit
