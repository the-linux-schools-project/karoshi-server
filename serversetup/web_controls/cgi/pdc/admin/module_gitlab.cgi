#!/bin/bash
#Copyright (C) 2015 Matthew Jowett
#
#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Setup Gitlab"
TITLEHELP=$"This will setup a Gitlab Server (CE 13.3 Omnibus)."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Gitlab_Server"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-%')
#########################
#Assign data to variables
#########################
END_POINT=9
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign SERVERNAME
DATANAME=SERVERNAME
get_data
SERVERNAME="$DATAENTRY"

#Assign ALIAS
DATANAME=ALIAS
get_data
ALIAS="${DATAENTRY//./}"

[ ! -z  "$SERVERNAME" ] && TITLE="$TITLE - $SERVERNAME"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/karoshi_servers_view.cgi"
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER": /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that ALIAS is not blank
if [ -z "$ALIAS" ]; then
	MESSAGE=$"You have not entered an alias or domain path."
	show_status
fi

#Check to see that SERVERNAME is not blank
if [ -z "$SERVERNAME" ]
then
	MESSAGE=$"The server cannot be blank."
	show_status
fi

#Check to see that the alias has not been assigned to another server.
for AliasInfo in $(grep "$ALIAS" -rl /opt/karoshi/server_network/aliases)
do
	AssignedServer=$(basename "$AliasInfo")
	if [ "$AssignedServer" != "$SERVERNAME" ]
	then
		MESSAGE=$"The alias you have chosen is already assigned to another server."
		show_status
	fi
done

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/module_gitlab.cgi | cut -d' ' -f1)
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$ALIAS:$SERVERNAME:" | sudo -H /opt/karoshi/web_controls/exec/module_gitlab
if [ "$?" = 101 ]
then
	MESSAGE=''$"There was a problem with this action."' '$"Please check the karoshi web administration logs for more details."''
	show_status
fi
echo '
</display-karoshicontent>
</body>
</html>'
exit
