#!/bin/bash
#Copyright (C) 2007 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

#Language
TITLE=$"Edit a Remote Management User"
ERRORMSG1="The user action must not be blank."
ERRORMSG2="You cannot delete yourself."
HTTPS_ERROR="You must access this page via https."
ACCESS_ERROR1="You must be a Karoshi Management User to complete this action."

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-%*+-' | sed 's/*/%1123/g' | sed 's/____/QUADRUPLEUNDERSCORE/g' | sed 's/_/REPLACEUNDERSCORE/g' | sed 's/QUADRUPLEUNDERSCORE/_/g')
#########################
#Assign data to variables
#########################
END_POINT=16
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign JOBTITLE
DATANAME=JOBTITLE
get_data
JOBTITLE="$DATAENTRY"

#Assign FORENAME
DATANAME=FORENAME
get_data
FORENAME="$DATAENTRY"

#Assign SURNAME
DATANAME=SURNAME
get_data
SURNAME="$DATAENTRY"

#Assign USERNAME
DATANAME=USERNAME
get_data
USERNAME="$DATAENTRY"

#Assign password1
DATANAME=PASSWORD1
get_data
PASSWORD1="$DATAENTRY"

#Assign password2
DATANAME=PASSWORD2
get_data
PASSWORD2="$DATAENTRY"

#Assign PRIMARYADMIN
DATANAME=PRIMARYADMIN
get_data
PRIMARYADMIN="$DATAENTRY"

#Assign TCPACCESS
DATANAME=TCPACCESS
get_data
TCPACCESS="$DATAENTRY"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
</script>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^$REMOTE_USER: /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that username is not blank
if [ -z "$USERNAME" ]
then
	MESSAGE=$"The username must not be blank."
	show_status
fi

#Check to see that password fields are not blank
if [ -z "$PASSWORD1" ] || [ -z "$PASSWORD2" ]
then
	if [ "$PASSWORD1"'check' != "$PASSWORD2"'check' ]
	then
		MESSAGE=$"The passwords do not match."
		show_status
	fi
fi

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/remote_management_edit.cgi | cut -d' ' -f1)
#add remote management user
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$JOBTITLE:$FORENAME:$SURNAME:$USERNAME:$PASSWORD1:$PRIMARYADMIN:$TCPACCESS" | sudo -H /opt/karoshi/web_controls/exec/remote_management_edit
EXEC_STATUS="$?"
MESSAGE=`echo $USERNAME $"has been edited."`
if [ "$EXEC_STATUS" = 103 ]
then
	MESSAGE=$"You can only do this if you are a primary admin."
	show_status
fi
if [ "$EXEC_STATUS" = 102 ]
then
	MESSAGE="$USERNAME: "$"This user does not exist."
	show_status
fi
if [ "$EXEC_STATUS" = 101 ]
then
	MESSAGE="$USERNAME: "$"There was a problem editing the details for this user."
	show_status
fi
echo '
<script>
	window.location = "remote_management_view.cgi";
</script>
</body>
</html>
'
exit
