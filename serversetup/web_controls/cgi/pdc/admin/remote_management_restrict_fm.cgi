#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

TITLE=$"Restrict access to the Web Management"
TITLEHELP=$"Access can be restricted by a complete tcpip number or by a partial number for a range."' '$"For example 172.30.4. will allow all computers from 172.30.4.1 to 172.30.4.254."
HELPURL="#"

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

TITLE=$"Restrict Access"
TITLEHELP=$"Restrict access to Karoshi Remote Management"' '$"Access can be restricted by a complete tcpip number or by a partial number for a range."' '$"For example 172.30.4. will allow all computers from 172.30.4.1 to 172.30.4.254."
HELPURL="#"

echo '
<form action="/cgi-bin/admin/remote_management_restrict.cgi" method="post">
	<button class="button" name="_RestrictAccess_" value="_">
		'$"View"'
	</button>
</form>
<form action="/cgi-bin/admin/remote_management_restrict.cgi" method="post">
	<input name="_ACTION_" value="add" type="hidden">
	<table>
		<tbody>
			<tr>
				<td class="karoshi-input">'$"Allowed tcpip address"'</td>
				<td><input class="karoshi-input" maxlength="15" name="_ADDTCPADDRESS_" size="15" type="text"></td>
			</tr>
			<tr>
				<td>'$"Comment"'</td>
				<td><input class="karoshi-input" maxlength="30" name="_TCPCOMMENT_" size="15" type="text"></td>
			</tr>
			<tr>
				<td>'$"Admin Level"'</td>
				<td>
					<select class="karoshi-input" name="_PRIMARYADMIN_">
						<option>1</option>
						<option>2</option>
						<option>3</option>
					</select>
				</td>
			</tr>
		</tbody>
	</table>
	<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</form>
</display-karoshicontent>
</body>
</html>
'
exit
