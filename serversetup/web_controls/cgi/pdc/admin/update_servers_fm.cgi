#!/bin/bash
#Copyright (C) 2009  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Update Servers"
TITLEHELP=$"This allows you to schedule updates for your servers."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Update_Servers"
TOOLTIPS=inforight

HOUR=$(date +%H)
MINUTES=$(date +%M)
SECONDS=$(date +%S)

#Detect mobile browser
MOBILE=no
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#########################
#Get data input
#########################

DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-%+')

END_POINT=9
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign _DAY_
DATANAME=DAY
get_data
DAY="$DATAENTRY"

#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"
	
ICON1=/images/submenus/system/logs.png

echo '
<form action="update_servers_view_logs_fm.cgi" name="tstest" method="post">
	<button class="button" name="_ViewUpdateLogs_" value="_">
		'$"Update Logs"'
	</button>
</form>'

#Preselect day
if [ ! -z "$DAY" ]
then
	[ "$DAY" = 1 ] && OP1='selected="selected"'
	[ "$DAY" = 2 ] && OP2='selected="selected"'
	[ "$DAY" = 3 ] && OP3='selected="selected"'
	[ "$DAY" = 4 ] && OP4='selected="selected"'
	[ "$DAY" = 5 ] && OP5='selected="selected"'
	[ "$DAY" = 6 ] && OP6='selected="selected"'
	[ "$DAY" = 7 ] && OP7='selected="selected"'
	[ "$DAY" = 8 ] && OP8='selected="selected"'
	[ "$DAY" = 9 ] && OP9='selected="selected"'
	[ "$DAY" = 10 ] && OP10='selected="selected"'
	[ "$DAY" = 11 ] && OP11='selected="selected"'
	[ "$DAY" = 12 ] && OP12='selected="selected"'
fi

echo '
<form action="/cgi-bin/admin/update_servers.cgi" name="tstest" method="post">
<table>
	<tbody>
		<tr>
			<td class="karoshi-input">'$"Day"'</td>
			<td>
				<select required="required" tabindex= "2" class="karoshi-input" name="_DAY_">
					<option label="blank" value=""></option>
					<option value="never">'$"Never"'</option>
					<option '"$OP1"' value="1">'$"Monday"'</option>
					<option '"$OP2"' value="2">'$"Tuesday"'</option>
					<option '"$OP3"' value="3">'$"Wednesday"'</option>
					<option '"$OP4"' value="4">'$"Thursday"'</option>
					<option '"$OP5"' value="5">'$"Friday"'</option>
					<option '"$OP6"' value="6">'$"Saturday"'</option>
					<option '"$OP7"' value="7">'$"Sunday"'</option>
					<option '"$OP8"' value="8">'$"Monday to Friday"'</option>
					<option '"$OP9"' value="9">'$"Monday to Saturday"'</option>
					<option '"$OP10"' value="10">'$"Monday, Wednesday, and Friday"'</option>
					<option '"$OP11"' value="11">'$"Tuesday and Thursday"'</option>
					<option '"$OP12"' value="12">'$"Tuesday, Thursday, and Saturday"'</option>
					<option '"$OP13"' value="13">'$"Every day"'</option>
				</select>
			</td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Update_Servers#Scheduling_Server_Updates"><span>'$"Choose the day that you want your servers to update on."'</span></a></td>
		</tr>
		<tr>
			<td>'$"Hour"'</td>
			<td><input required="required" min="0" max="24" tabindex= "2" value="'"$HOUR"'" name="_HOURS_" class="karoshi-input" size="3" type="number"></td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Update_Servers#Scheduling_Server_Updates"><span>'$"Choose the time that you want your servers to update on."'</span></a></td>
		</tr>
		<tr>
			<td>'$"Minutes"'</td>
			<td><input required="required" min="0" max="60" tabindex= "3" value="'"$MINUTES"'" name="_MINUTES_" class="karoshi-input" size="3" type="number"></td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Update_Servers#Scheduling_Server_Updates"><span>'$"Choose the time that you want your servers to update on."'</span></a></td>
		</tr>
		<tr>
			<td>'$"Force Reboot"'</td>
			<td><input id="ForceReboot" type="checkbox" name="_FORCEREBOOT_" value="yes"><label for="ForceReboot"></label></td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Update_Servers#Scheduling_Server_Updates"><span>'$"Select this option only if you have a server that will not restart correctly if a reboot is required after system updates."'</span></a></td>
		</tr>
	</tbody>
</table>'

#Show list of servers
/opt/karoshi/web_controls/show_servers "$MOBILE" all $"Schedule Update" notset updateserver

echo '
</form>
</display-karoshicontent>
</body>
</html>'
exit

