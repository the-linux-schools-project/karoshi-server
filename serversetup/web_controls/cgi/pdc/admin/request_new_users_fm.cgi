#!/bin/bash
#Copyright (C) 2009  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Requested Users"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

if [ -d /opt/karoshi/user_requests/new_users ]
then
	if [[ $(ls -1 /opt/karoshi/user_requests/new_users | wc -l) -gt 0  ]]
	then
		echo '
		<table>
			<thead>
				<tr>
					<th class="karoshi-input"><b>'$"Forename"'</b></th>
					<th class="karoshi-input"><b>'$"Surname"'</b></th>
					<th class="karoshi-input"><b>'$"Primary Group"'</b></th>
					<th class="karoshi-input"><b>'$"Admission Number"'</b></th>
					<th class="karoshi-input"><b>'$"Requested by"'</b></th>
					<th><b>'$"Add"'</b></th>
					<th><b>'$"Remove"'</b></th>
				</tr>
			</thead>
			<tbody>'
		for NEW_USER in /opt/karoshi/user_requests/new_users/*
		do
			NEW_USER_DATA=$(sed -n 1,1p "$NEW_USER")
			FORENAME=$(echo "$NEW_USER_DATA" | cut -d: -f1)
			SURNAME=$(echo "$NEW_USER_DATA" | cut -d: -f2)
			GROUP=$(echo "$NEW_USER_DATA" | cut -d: -f3)
			ENROLLMENTNUMBER=$(echo "$NEW_USER_DATA" | cut -d: -f4)
			REQUESTUSER=$(echo "$NEW_USER_DATA" | cut -d: -f5)
			FILE=$(basename "$NEW_USER")
			echo '
				<tr>
					<td>'"$FORENAME"'</td>
					<td>'"$SURNAME"'</td>
					<td>'"$GROUP"'</td>
					<td>'"$ENROLLMENTNUMBER"'</td>
					<td>'"$REQUESTUSER"'</td>
					<td>
						<form style="display: inline;" action="/cgi-bin/admin/add_user_fm.cgi" method="post">
							<button class="info" name="_AddUser_" value="_ACTION_'"$FILE"'_">
								<img src="/images/submenus/user/adduser.png" alt="'$"Add User"'">
								<span>'$"Add User"'</span>
							</button>
						</form>
					</td>
					<td>
						<form style="display: inline;" action="/cgi-bin/admin/request_new_users_delete.cgi" method="post">
							<button class="info" name="_DeleteUser_" value="_ACTION_'"$FILE"'_">
								<img src="/images/submenus/user/delete_user.png" alt="'$"Delete"'">
								<span>'$"Delete"'</span>
							</button>
						</form>
					</td>
				</tr>'
		done
		echo '
			</tbody>
		</table>'
	else
		echo $"All requested users have been dealt with."'<br>'
	fi
fi
echo '
</body>
</html>'
exit
