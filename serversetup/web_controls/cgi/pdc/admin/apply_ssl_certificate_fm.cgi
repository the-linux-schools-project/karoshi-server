#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Apply SSL Certificate"
TITLEHELP=$"This will create an ssl certificate signed by the root signing authority on your server."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=SSL_Certificate"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '<form action="/cgi-bin/admin/apply_ssl_certificate.cgi" name="selectservers" method="post">
 
<table id="myTable" class="tablesorter">
	<thead>
		<tr>
			<th class="karoshi-input"><b>Server</b></th>
			<th><b>Alias</b></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>'"$HOSTNAME"'</td>
			<td>manage.'"$REALM"'</td>
			<td style="text-align:right">
				<button class="button" name="_ApplyCert_" value="_SERVER_'"$HOSTNAME"'_">
					'$"Apply Certificate"'
				</button>
			</td>
		</tr>
		<tr>
			<td>'$"All Web Servers"'</td>
			<td>*.'"$REALM"'</td>
			<td style="text-align:right">
				<button class="button" name="_ApplyCertAllServers_" value="_SERVER_allwebservers_">
					'$"Apply Certificate"'
				</button>
			</td>
		</tr>
	</tbody>
</table>
</form>
</display-karoshicontent>
</body>
</html>
'


exit

