#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Protected Distribution Lists"
TITLEHELP=$"Only users added to the list can send E-mails to protected distribution lists."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=E-Mail_Protected_Distribution_Lists"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
END_POINT=10
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

#Assign GROUP
DATANAME=GROUP
get_data
GROUP="$DATAENTRY"

#Assign USERNAME
DATANAME=USERNAME
get_data
USERNAME="$DATAENTRY"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/email_protected_distribution_lists.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"The action cannot be blank."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
[ -z "$ACTION" ] && ACTION=view
if [ "$ACTION" = delete ] || [ "$ACTION" = reallyadd ]
then
	if [ -z "$GROUP" ]
	then
		MESSAGE=$"You have not entered a username."
		show_status
	fi
fi

if [ "$ACTION" = viewusers ] || [ "$ACTION" = adduser ]
then
	ACTION2=view
	ICON=$ICON3
	MESSAGE=$"Protected Distribution Lists"
	MESSAGE2=$"View the protected distribution lists."
	HELPMSG=$"Adding a protected domain restricts which internal users can send to it."
else
	ACTION2=viewusers
	ICON=$ICON2
	MESSAGE=$"Allowed Users"
	MESSAGE2=$"View allowed users."
	HELPMSG=$"Only users added to the list can send E-mails to these distribution lists."
fi

echo '
<form action="/cgi-bin/admin/email_protected_distribution_lists.cgi" method="post">
	<button class="button" name="_DoAction_" value="_ACTION_'"$ACTION2"'_">
		'"$MESSAGE"'
	</button>
</form>'

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/email_protected_distribution_lists.cgi | cut -d' ' -f1)
#Show aliases
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$ACTION:$USERNAME:$GROUP:$MOBILE:" | sudo -H /opt/karoshi/web_controls/exec/email_protected_distribution_lists

echo '</display-karoshicontent>
</body>
</html>'
