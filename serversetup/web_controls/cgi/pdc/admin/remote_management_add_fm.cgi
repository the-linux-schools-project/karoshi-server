#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Add Web Management User"
TITLEHELP=$"These accounts are used by your technical staff to access the web managagement. The usernames and passwords used here are totally separate from normal network users."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_a_Remote_Admin"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "
<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<form action="/cgi-bin/admin/remote_management_view.cgi" method="post">
	<button class="button" formaction="remote_management_view.cgi" name="_ViewUsers_" value="_">
		'$"View"'
	</button>
</form>

<form action="/cgi-bin/admin/remote_management_add.cgi" method="post">

<table>
	<tbody>
		<tr>
			<td class="karoshi-input">Job Title</td>
			<td><input class="karoshi-input" name="_JOBTITLE_" tabindex="1" class="karoshi-input" size="20" type="text"></td>
			<td></td>
		</tr>
		<tr>
			<td>Forename</td>
			<td><input class="karoshi-input" name="_FORENAME_" tabindex="2" class="karoshi-input" size="20" type="text"></td><td></td>
		</tr>
		<tr>
			<td>Surname</td>
			<td><input class="karoshi-input" name="_SURNAME_" tabindex="3" class="karoshi-input" size="20" type="text"></td><td></td>
		</tr>
		<tr>
			<td>'$"Username"'</td>
			<td><input required="required" tabindex="4" name="_USERNAME_" class="karoshi-input" size="20" type="text"></td>
			<td></td>
		</tr>
		<tr>
			<td>'$"Password"'</td>
			<td><input required="required" tabindex="5" name="_PASSWORD1_" class="karoshi-input" size="20" type="password"></td><td></td>
		</tr>
		<tr>
			<td>'$"Confirm"'</td>
			<td><input required="required" tabindex="6" name="_PASSWORD2_" class="karoshi-input" size="20" type="password"></td><td></td>
		</tr>
		<tr>
			<td>'$"Access Level"'</td>
			<td>
				<select required="required" tabindex="7" name="_PRIMARYADMIN_" class="karoshi-input">
					<option label=" " value=""></option>
					<option value="1">'$"Primary Admin"'</option>
					<option value="2">'$"Admin"'</option>
					<option value="3">'$"Technician"'</option>        
				</select>
			</td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_a_Remote_Admin"><span class="icon-large-tooltip">'$"Primary Admins have full control of the web management."'<br><br>'$"Admins have full control of the web management but cannot add or delete other admins."'<br><br>'$"Technicians can access a limited set of controls for day to day running of the system such as changing passwords."'</span></a></td>
		</tr>
	</tbody>
</table>
<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</form>
</display-karoshicontent>
</body>
</html>
'
exit
