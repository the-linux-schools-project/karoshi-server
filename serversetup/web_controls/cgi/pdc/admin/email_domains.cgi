#!/bin/bash
#Copyright (C) 2009  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"E-Mail Domains"
TITLEHELP=$"This shows any domains that you have set up for your E-Mail system."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=E-Mail_Domains"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"	

DATA=$(cat | tr -cd 'A-Za-z0-9\._:%\-+')

#########################
#Assign data to variables
#########################
END_POINT=15
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

#Assign DOMAIN
DATANAME=DOMAIN
get_data
DOMAIN="$DATAENTRY"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/email_domains.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

if [ -z "$ACTION" ]
then
	ACTION=view
fi

ALTACTION=add
ALTMESSAGE=$"Add Domain"
ALTMESSAGE2=$"Add a domain."
ICON="$ICON1"
if [ "$ACTION" = add ]
then
	ALTACTION=view
	ALTMESSAGE=$"Domains"
	ALTMESSAGE2=$"View the E-Mail domains."
	ICON="$ICON2"
fi

#########################
#Check data
#########################
if [ "$ACTION" != add ] && [ "$ACTION" != reallyadd ] && [ "$ACTION" != reallydelete ] && [ "$ACTION" != delete ] && [ "$ACTION" != view ]
then
	MESSAGE=$"You have not entered a correct action."
	show_status
fi

if [ "$ACTION" = reallyadd ] || [ "$ACTION" = reallydelete ]
then
	#Make sure that the domain is not blank
	if [ -z "$DOMAIN" ]
	then
		MESSAGE=$"The domain must not be blank."
		show_status
	fi
fi

if [ "$ACTION" = reallyadd ]
then
	source /opt/karoshi/server_network/domain_information/domain_name
	#Make sure that the domain is not the real domain
	if [ "$DOMAIN" = "$REALM" ]
	then
		MESSAGE=$"You cannot add the main domain as a virtual domain."
		show_status
	fi
	
	#Check that a domain has been entered
	if [[ $(echo "$DOMAIN" | grep -c "\.") = 0 ]]
	then
		MESSAGE=$"You have not entered in a valid domain."
		show_status
	fi
fi

echo '
<form action="/cgi-bin/admin/email_domains.cgi" method="post">
	<button class="button" name="_DoAction_" value="_ACTION_'"$ALTACTION"'_">
		'"$ALTMESSAGE"'
	</button>

	<button formaction="/cgi-bin/admin/email_aliases.cgi" class="button" name="_ViewAliases_" value="_">
		'$"Aliases"'
	</button>
</form>'

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/email_domains.cgi | cut -d' ' -f1)
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$MOBILE:$ACTION:$DOMAIN:" | sudo -H /opt/karoshi/web_controls/exec/email_domains

echo '
</display-karoshicontent>
</body>
</html>'
exit
