#!/bin/bash
#Copyright (C) 2010  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Windows Commands"
TITLEHELP=$"This will send commands to a windows machine joined to your network."
HELPURL="#"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\+%-')
#########################
#Assign data to variables
#########################
END_POINT=15
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign SERVER
DATANAME=SERVER
get_data
SERVER="$DATAENTRY"

#Assign COMMAND
DATANAME=COMMAND
get_data
COMMAND="$DATAENTRY"


#Assign OPTIONS
DATANAME=OPTIONS
get_data
OPTIONS="$DATAENTRY"

function show_status {
echo '<SCRIPT language="Javascript">'
echo 'alert("'"$MESSAGE"'");'
echo 'window.location = "/cgi-bin/admin/windows_machine_commands_fm.cgi";'
echo '</script>'
echo "</display-karoshicontent></body></html>"
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that server is not blank
if [ -z "$SERVER" ]
then
	MESSAGE=$"The server cannot be blank."
	show_status
fi
#Check to see that the command is not blank
if [ -z "$COMMAND" ]
then
	MESSAGE=$"The command cannot be blank."
	show_status
fi


[ "$COMMAND" = startservice ] && COMMAND2=$"Start service"
[ "$COMMAND" = stopservice ] && COMMAND2=$"Stop service"
[ "$COMMAND" = servicestatus ] && COMMAND2=$"Service status"
[ "$COMMAND" = shutdown ] && COMMAND2=$"Shutdown"
[ "$COMMAND" = restart ] && COMMAND2=$"Restart"
[ "$COMMAND" = abortshutdown ] && COMMAND2=$"Abort shutdown"
[ "$COMMAND" = showprinters ] && COMMAND2=$"Show printers"
[ "$COMMAND" = showshares ] && COMMAND2=$"Show shares"
[ "$COMMAND" = showfiles ] && COMMAND2=$"Show open files"

echo '<b>'$"Windows Commands"' - '"$COMMAND2"' '"$TCPIP"'</b><br><br>'

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/windows_machine_commands.cgi | cut -d' ' -f1)
#Run command
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$SERVER:$COMMAND:$OPTIONS:" | sudo -H /opt/karoshi/web_controls/exec/windows_machine_commands

echo '
</display-karoshicontent>
</body>
</html>'


exit
