#!/bin/bash
#Copyright (C) 2014  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Windows Printer Drivers"
TITLEHELP=$"This is used to enable or disable automated Windows printer driver generation for your print queues."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Printer_Driver_Generation"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "
<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
END_POINT=7
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign action
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

#Assign queue
DATANAME=QUEUE
get_data
QUEUE="$DATAENTRY"

[ -z "$ACTION" ] && ACTION=view

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/printer_driver_gen.cgi";
</script>
</form>
</display-karoshicontent>
</body>
</html>'
exit
}

echo '
<form action="/cgi-bin/admin/printer_driver_gen.cgi" method="post">

	<button class="button" name="_EnableAll_" value="_ACTION_enableall_PRINTQUEUE_all_">
		'$"Enable All"'
	</button>

	<button class="button" name="_DisableAll_" value="_ACTION_disableall_PRINTQUEUE_all_">
		'$"Disable all"'
	</button>

	<button class="button" name="_GenerateDrivers_" value="_ACTION_gendrivers_PRINTQUEUE_all_">
		'$"Generate Drivers"'
	</button>

	<button formaction="printers.cgi" class="button" name="_ShowPrinters_" value="_">
		'$"Show Printers"'
	</button>
</form>
<form action="/cgi-bin/admin/printer_driver_gen.cgi" name="selectedsites" method="post">
'



Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/printer_driver_gen.cgi | cut -d' ' -f1)
if [ "$ACTION" = gendrivers ]
then
	sudo -H /opt/karoshi/web_controls/exec/printer_driver_gen2 "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$MOBILE:"
	EXEC_STATUS="$?"
	[ "$EXEC_STATUS" = 0 ] && MESSAGE=$"Windows printer driver generation completed."

	if [ "$EXEC_STATUS" = 102 ]
	then
		MESSAGE=$"The samba root password was incorrect."
	fi
	if [ "$EXEC_STATUS" = 103 ]
	then
		MESSAGE=$"The windows dll files needed to generate the windows printer drivers are missing. The dlls needed in /usr/share/cups/drivers/ are pscript5.dll, ps5ui.dll, pscript.hlp, pscript.ntf."
	fi
	show_status
else
	#Check that we have some printer queues
	COUNTER=$(grep -n ^--start-- /var/lib/samba/netlogon/printers.txt | cut -d: -f1)
	let COUNTER="$COUNTER"+1
	NOOFLINES=$(wc -l < /var/lib/samba/netlogon/printers.txt)
	if [ "$COUNTER" -lt "$NOOFLINES" ]
	then
		echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$ACTION:$QUEUE:$MOBILE:" | sudo -H /opt/karoshi/web_controls/exec/printer_driver_gen
	else
		echo '<ul><li>'$"No Printers have been assigned"'</li></ul>'
	fi
fi

echo '
</display-karoshicontent>
</form>
</body>
</html>'
exit
