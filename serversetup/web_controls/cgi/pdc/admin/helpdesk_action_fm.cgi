#!/bin/bash
#Copyright (C) 2010  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Technical Support Action Request"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\.%+_:\-')
#########################
#Assign data to variables
#########################
END_POINT=5
#Assign JOBNAME
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = JOBNAMEcheck ]
	then
		let COUNTER=$COUNTER+1
		JOBNAME=`echo $DATA | cut -s -d'_' -f$COUNTER`
		break
	fi
	let COUNTER=$COUNTER+1
done

function show_status {
echo '
<script>
	alert("'$MESSAGE'");
	window.location = "/cgi-bin/admin/helpdesk_view_fm.cgi";
</script>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi

#########################
#Check data
#########################
#Check to see that JOBNAME is not blank
if [ -z "$JOBNAME" ]
then
	MESSAGE=$"The job name cannot be blank."
	show_status
fi

if [ ! -f /opt/karoshi/server_network/helpdesk/todo/$JOBNAME ]
then
	MESSAGE=$"This job does not exist."
	show_status
fi

#Generate navigation bar
if [ $MOBILE = no ]
then
	WIDTH1=180
	WIDTH2=200
	WIDTH3=600
	WIDTH4=192
	HEIGHT1=24
	COLS=70
	ROWS=6
else
	WIDTH1=140
	WIDTH2=160
	WIDTH3=160
	WIDTH4=160
	HEIGHT1=30
	COLS=18
	ROWS=4
fi

#Get data
source /opt/karoshi/server_network/helpdesk/"todo/$JOBNAME"

#Show job data

echo '
<form action="/cgi-bin/admin/helpdesk_action.cgi" method="post">
	<input name="_JOBNAME_" value="'$JOBNAME'" type="hidden">
	<table>
		<tbody>
			<tr>
				<td class="karoshi-input">'$"Request Summary"'</td>
				<td>'"$JOBTITLE"'</td>
			</tr>
			<tr>
				<td>'$"Name"'</td>
				<td>'"$NAME"'</td>
			</tr>
			<tr>
				<td>'$"Location"'</td>
				<td>'"$LOCATION"'</td>
			</tr>
			<tr>
				<td>'$"Department"'</td>
				<td>'"$DEPARTMENT"'</td>
			</tr>
			<tr>
				<td>'$"Category"'</td>
				<td>'"$CATEGORY"'</td>
			</tr>
			<tr>
				<td>'$"Extended Details"'</td>
				<td>'$REQUEST'</td>
			</tr>
			<tr>
				<td>'$"Update"'</td>
				<td><input id="Update" tabindex= "1" name="_ACTION_"  checked="checked" value="update" type="radio"><label for="Update"> </label></td>
			</tr>
			<tr>
				<td>'$"Delete"'</td>
				<td><input id="Delete" tabindex= "2" name="_ACTION_" value="delete" type="radio"><label for="Delete"> </label></td>
			</tr>
			<tr>
				<td>'$"Completed"'</td>
				<td><input id="Completed" tabindex= "3" name="_ACTION_" value="completed" type="radio"><label for="Completed"> </label></td>
			</tr>
			<tr>
				<td>'$"Assigned to"'</td>
				<td>'
#Show list of technical staff
echo '
					<select class="karoshi-input" tabindex= "4" name="_ASSIGNED_">
						<option label="CurrentAssigned">'$ASSIGNED'</option>'
cat /opt/karoshi/web_controls/web_access_admin | cut -d: -f1 | sed 's/^/<option>/g' | sed 's/$/<\/option>/g'
cat /opt/karoshi/web_controls/web_access_tech | cut -d: -f1 | sed 's/^/<option>/g' | sed 's/$/<\/option>/g'
echo '
					</select>'

echo '
				</td>
			</tr>
			<tr>
				<td>'$"Alternative assign"'</td>
				<td><input class="karoshi-input" tabindex= "5" name="_ASSIGNED2_" size="20" type="text"></td>
			</tr>
			<tr>
				<td>'$"Priority"'</td>
				<td>
					<select class="karoshi-input" tabindex= "6" name="_PRIORITY_">
						<option label="CurrentPriority" value="'$PRIORITY'">'$PRIORITY'</option>
						<option value="'$"Urgent"'">'$"Urgent"'</option>
						<option value="'$"High"'">'$"High"'</option>
						<option value="'$"Medium"'">'$"Medium"'</option>
						<option value="'$"Low"'">'$"Low"'</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>'$"Feedback"'</td>
				<td><textarea style="width: '$WIDTH3'px;" tabindex= "7" cols="'$COLS'" rows="'$ROWS'" name="_FEEDBACK_">'$FEEDBACK'</textarea></td>
			</tr>
		</tbody>
	</table>
	<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</form>
</body>
</html>'
exit

