#!/bin/bash
#Copyright (C) 2010  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"View Monitor Logs"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\+-')
#########################
#Assign data to variables
#########################
END_POINT=9
#Assign GROUPNAME
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = GROUPNAMEcheck ]
	then
		let COUNTER=$COUNTER+1
		GROUPNAME=`echo $DATA | cut -s -d'_' -f$COUNTER`
		break
	fi
	let COUNTER=$COUNTER+1
done
#Assign SERVICE
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = SERVICEcheck ]
	then
		let COUNTER=$COUNTER+1
		SERVICE=`echo $DATA | cut -s -d'_' -f$COUNTER`
		break
	fi
	let COUNTER=$COUNTER+1
done

function show_status {
echo '<SCRIPT language="Javascript">
	alert("'$MESSAGE'");
	window.location = "mon_status.cgi"
</script>
</display-karoshicontent>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that GROUPNAME is not blank
if [ -z "$GROUPNAME" ]
then
	MESSAGE=$"The groupname cannot be blank."
	show_status
fi

#Check to see that SERVICE is not blank
if [ -z "$SERVICE" ]
then
	MESSAGE=$"The service cannot be blank."
	show_status
fi
ICON=/images/submenus/system/computer.png


echo '
<form action="/cgi-bin/admin/mon_status.cgi" method="post">
	<button class="button" name="_MonStatus_" value="_">
		'$"Status"'
	</button>
</form>
<table>
	<tbody>
		<tr>
			<td class="karoshi-input">'$"Group Name"'</td>
			<td>'"$GROUPNAME"'</td>
		</tr>
		<tr>
			<td>'$"Service"'</td>
			<td>'$SERVICE'</td>
		</tr>
	</tbody>
</table>'


Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/monitors_view_logs.cgi | cut -d' ' -f1)
#Show monitor logs
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$GROUPNAME:$SERVICE:" | sudo -H /opt/karoshi/web_controls/exec/monitors_view_logs

if [ "$?" = 101 ]
then
	MESSAGE=$"No Monitor Server has been set up."
	show_status
fi
echo '
</display-karoshicontent>
</body>
</html>'
exit

