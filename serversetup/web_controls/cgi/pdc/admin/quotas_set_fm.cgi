#!/bin/bash
#Copyright (C) 2008  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Apply Quota Settings"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#########################
#Get data input
#########################

DATA=`cat | tr -cd 'A-Za-z0-9\._:\-'`
#########################
#Assign data to variables
#########################
END_POINT=10
#Assign GROUP
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = GROUPcheck ]
	then
		let COUNTER=$COUNTER+1
		GROUP=`echo $DATA | cut -s -d'_' -f$COUNTER`
		break
	fi
	let COUNTER=$COUNTER+1
done
#Assign SIZE
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = SIZEcheck ]
	then
		let COUNTER=$COUNTER+1
		SIZE=`echo $DATA | cut -s -d'_' -f$COUNTER | tr -cd '0-9/n'`
		break
	fi
	let COUNTER=$COUNTER+1
done

[ -z $SIZE ] && SIZE=300

#Assign MAXFILES
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = MAXFILEScheck ]
	then
		let COUNTER=$COUNTER+1
		MAXFILES=`echo $DATA | cut -s -d'_' -f$COUNTER`
		break
	fi
	let COUNTER=$COUNTER+1
done

[ -z $MAXFILES ] && MAXFILES=2000

#Assign UNIT
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = UNITcheck ]
	then
		let COUNTER=$COUNTER+1
		UNIT=`echo $DATA | cut -s -d'_' -f$COUNTER`
		break
	fi
	let COUNTER=$COUNTER+1
done
#Assign USERNAME
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = USERNAMEcheck ]
	then
		let COUNTER=$COUNTER+1
		USERNAME=`echo $DATA | cut -s -d'_' -f$COUNTER`
		break
	fi
	let COUNTER=$COUNTER+1
done

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<form action="/cgi-bin/admin/quotas_view_usage_fm.cgi" name="selectservers" method="post">
	<button class="button" name="_QuotaUsage_" value="_">
		'$"Quota Usage"'
	</button>

	<button class="button" formaction="quotas_view_partitions.cgi" name="_EnabledPartitions_" value="_">
		'$"Enabled Partitions"'
	</button>

</form>
<form action="/cgi-bin/admin/quotas_set.cgi" name="selectservers" method="post">
	<table>
		<tbody>
			<tr>
				<td class="karoshi-input">'$"Username"'</td>
 				<td><input class="karoshi-input" size="20" name="_USERNAME_" value="'$USERNAME'" type="text"></td>
				<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Disk_Quotas#Quota_Settings"><span class="icon-large-tooltip">'$"Enter in the username that you want to apply these settings to."'<br><br>'$"Leave this field blank if you want to apply the settings to a whole group of users."'</span></a></td>
			</tr>
			<tr>
				<td>'$"Group"'</td>
 				<td>'

#Show groups
/opt/karoshi/web_controls/group_dropdown_list

echo '
				</td>
				<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Disk_Quotas#Quota_Settings"><span class="icon-large-tooltip">'$"Choose the group that you want to apply the quota settings to."'</span></a></td>
			</tr>
			<tr>
				<td>Unit</td>
				<td>
					<select class="karoshi-input" name="_UNIT_" class="karoshi-input">
						<option value="MB">MB</option> 
						<option value="GB" selected="selected">GB</option>
						<option value="TB">TB</option>
					</select>
				</td>
				<td></td>
			</tr>
			<tr>
				<td>'$"Size"'</td>
				<td><input class="karoshi-input" maxlength="8" name="_SIZE_" value="'$SIZE'" type="text"></td>
				<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Disk_Quotas#Quota_Settings"><span class="icon-large-tooltip">'$"This is the maximum file size that users can have on the system."'<br><br>'$"Users who reach this limit will not be able to save any more files until they have deleted some files."'<br><br>'$"Setting this value to 0 will disable this option."'</span></a>
</td>
			</tr>
			<tr>
				<td>'$"Max Files"'</td>
				<td><input class="karoshi-input" maxlength="8" name="_MAXFILES_" value="'$MAXFILES'" type="text"></td>
				<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Disk_Quotas#Quota_Settings"><span class="icon-large-tooltip">'$"This is the maximum number of files that a user can save on the system."'<br><br>'$"Users who reach this limit will not be able to save any more files until they have deleted some files."'<br><br>'$"Setting this value to 0 will disable this option."'</span></a></td>
			</tr>
		</tbody>
	</table>
	<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</form>
</display-karoshicontent>
</body>
</html>
'
exit

