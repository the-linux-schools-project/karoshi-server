#!/bin/bash
#Copyright (C) 2016  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Let's Encrypt SSL Certificates"
TITLEHELP=$"Apply a Let's Encrypt SSL certificate to a server."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=SSL_Let%27s_Encrypt"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-+')
#########################
#Assign data to variables
#########################
END_POINT=19
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign SERVERNAME
DATANAME=SERVERNAME
get_data
SERVERNAME="$DATAENTRY"

DATANAME=CUSTOMWEBADDRESS
get_data
CUSTOMWEBADDRESS="$DATAENTRY"

DATANAME=MODE
get_data
MODE="$DATAENTRY"

[ -z "$MODE" ] && MODE=auto

echo '<form action="/cgi-bin/admin/ssl_lets_encrypt.cgi" name="selectservers" method="post">'

if [ -z "$SERVERNAME" ]
then
	#Show custom alias box
	echo '
	<table>
		<tbody>
			<tr>
				<td class="karoshi-input">'$"Web Address Mode"'</td>
				<td style="width: 330px;">'
		if [ "$MODE" = auto ]
		then
			echo '
					<input name="_MODE_manual_" type="submit" class="button" value='$"Auto"'>'
		else
			echo '
					<input name="_MODE_auto_" type="submit" class="button" value='$"Manual"'>'
		fi
	echo '
				</td>
				<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=SSL_Let%27s_Encrypt"><span class="icon-large-tooltip">'$"In auto mode the web addresses of the server are automatically entered for the certificate based on the modules that have been applied to the server. Manual mode allows you to enter in custom web addresses for the certificate."'</span></a></td>
			</tr>'
	if [ "$MODE" = auto ]
	then
		echo '
			<tr>
				<td></td>
				<td></td>
				<td></td>
			</tr>'
	else
		echo '
			<tr>
				<td>'$"Custom Alias"'</td>
				<td><input class="karoshi-input" tabindex="1" name="_CUSTOMWEBADDRESS_" size="20" type="text"></td>
				<td style="vertical-align:bottom"><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=SSL_Let%27s_Encrypt"><span class="icon-large-tooltip">'$"This field should normally be left blank to automtically create the certificate for the server. Enter in a web address if you want to override the default web addresses of the server. Multiple web addresses can be entered separated by spaces."'</span></a></td>
			</tr>'
	fi
	echo '
		</tbody>
	</table>'

	#Show list of servers
	/opt/karoshi/web_controls/show_servers "$MOBILE" servers $"Apply"
else
	#Show any assigned aliases for the server.
	#Show alias choice
	source /opt/karoshi/server_network/domain_information/domain_name
	if [ -f /opt/karoshi/server_network/webservers/"$SERVERNAME" ]
	then
		#Show any custom aliases that have been assigned
		echo "<ul><li>$SERVERNAME" - $"Creating an SSl certificate for the following domain entries""</li></ul>"

		if [ -z "$CUSTOMWEBADDRESS" ]
		then
			for CUSTOM_ALIAS in $(cat /opt/karoshi/server_network/aliases/"$SERVERNAME")
			do
				echo "<ul><li>$CUSTOM_ALIAS.$REALM</li></ul>"
				ALIASLIST="$ALIASLIST,$CUSTOM_ALIAS.$REALM"
			done
			ALIASLIST=$(echo "$ALIASLIST" | sed 's/^,//g')
		else
			ALIASLIST="${CUSTOMWEBADDRESS//+/ }"
			for CUSTOM_ALIAS in $ALIASLIST
			do
				echo "<ul><li>$CUSTOM_ALIAS</li></ul>"
			done
			ALIASLIST="${CUSTOMWEBADDRESS// /,}"
		fi
		ACTION=addcert
		Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/ssl_lets_encrypt.cgi | cut -d' ' -f1)
		echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$SERVERNAME:$ALIASLIST:$ACTION:" | sudo -H /opt/karoshi/web_controls/exec/ssl_lets_encrypt
	else
		echo "<ul><li>$SERVERNAME: This server has not been set up as a web server</li></ul>"	
	fi
fi

echo '
</form>
</display-karoshicontent>
</body>
</html>'
exit
