#!/bin/bash
#Copyright (C) 2013  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Linux Client software packages"
TITLEHELP=$"Choose the linux client version that you want to set the software settings for."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Linux_Client_Software_Packages"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "
		<display-karoshicontent>
		<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
			<form action="/cgi-bin/admin/linux_client_install_software_packages.cgi" name="selectservers" method="post">'

#Show list of supported linux client versions

if [ -f /opt/karoshi/server_network/info ]
then
	source /opt/karoshi/server_network/info
	source /opt/karoshi/web_controls/version
	LOCATION_NAME="- $LOCATION_NAME"
fi
echo '
				<table>
					<tbody>
						<tr>
							<td class="karoshi-input">'$"Linux Client Version"'</td>
							<td><select name="_KAROSHIVERSION_" class="karoshi-input">'

for VERSION in $(cat /var/lib/samba/netlogon/linuxclient/versions.txt)
do
	echo '
								<option value="'"$VERSION"'">'"$VERSION"'</option>'	
done

echo '
							</select></td>
						</tr>
					</tbody>
				</table>
				<input value="'$"Submit"'" class="button primary" type="submit">'
echo '
			</form>
		</display-karoshicontent>
	</body>
</html>'
exit
