#!/bin/bash
#Copyright (C) 2014  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Radius Access Points"
TITLEHELP=$"Radius Access Points"
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Radius_Server#Viewing_Access_Points"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter({
	headers: {
	1: { sorter: "ipAddress" },
	2: { sorter: false},
	3: { sorter: false}
    		}
		});
    }
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"


#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-%' | sed 's/____/QUADRUPLEUNDERSCORE/g' | sed 's/_/REPLACEUNDERSCORE/g' | sed 's/QUADRUPLEUNDERSCORE/_/g')
#########################
#Assign data to variables
#########################
END_POINT=10
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

#Assign TCPIP
DATANAME=TCPIP
get_data
TCPIP="$DATAENTRY"

#Assign SHORTNAME
DATANAME=SHORTNAME
get_data
SHORTNAME="$DATAENTRY"

#Assign SECRETKEY
DATANAME=SECRETKEY
get_data
SECRETKEY="$DATAENTRY"


function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/radius_access_points.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#Check data
if [ "$ACTION" = reallyadd ]
then
	#Check to see that TCPIP is not blank
	if [ -z "$TCPIP" ]
	then
		MESSAGE=$"The tcpip number cannot be blank."
		show_status
	fi
	#Check to see that SHORTNAME is not blank
	if [ -z "$SHORTNAME" ]
	then
		MESSAGE=$"The shortname cannot be blank."
		show_status
	fi
	#Check to see that SECRETKEY fields are not blank
	if [ -z "$SECRETKEY" ]
	then
		MESSAGE=$"The secret key cannot be blank."
		show_status
	fi

	#Check that we have some sort of useful ip address.
	if [[ $(ipcalc -n "$TCPIP" | grep -c INVALID) -gt 0 ]]
	then
		MESSAGE=$"The TCPIP address is incorrect."
		show_status	
	fi

fi

[ -z "$ACTION" ] && ACTION=view

if [ "$ACTION" = add ] || [ "$ACTION" = edit ]
then
	ACTION2=view
	BUTTONTXT=$"View access points."
	TITLETXT=$"Add Access Point"
	BUTTONTXT2=$"View"
	CHOICEICON="$ICON2"
fi

if [ "$ACTION" = view ] || [ "$ACTION" = reallyadd ] || [ "$ACTION" = delete ]
then
	ACTION2=add
	BUTTONTXT=$"Add an access point."
	TITLETXT=$"Radius Access Points"
	BUTTONTXT2=$"Add"
	CHOICEICON="$ICON3"
fi



echo '
<form action="/cgi-bin/admin/radius_access_points.cgi" method="post">
	<button formaction="radius_access_controls.cgi" class="button" name="____ViewAccessControls____" value="____">
		'$"Access Controls"'
	</button>

	<button class="button" name="____ActionChoice____" value="____ACTION____'$ACTION2'____">
		'"$BUTTONTXT2"'
	</button>
</form>
<form action="/cgi-bin/admin/radius_access_points.cgi" method="post">'

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/radius_access_points.cgi | cut -d' ' -f1)
#View access points
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$MOBILE:$ACTION:$SHORTNAME:$SECRETKEY:$TCPIP:" | sudo -H /opt/karoshi/web_controls/exec/radius_access_points

echo '
</form>
</display-karoshicontent>
</body>
</html>'
exit
