#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Exam Accounts - Copy Data"
TITLEHELP=$"Select the exam accounts that you want to copy the data to."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts#Copy_Data_to_Exam_Accounts"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#Check to see if any data has been uploaded
if [[ $(ls -1 /var/www/karoshi/exam_upload | wc -l) = 0 ]]
then
	echo '
'$"No files have been uploaded to copy to the exam accounts."'
</display-karoshicontent></body></html>'
	exit
fi

ExamCount=$(getent passwd | grep -c /home/users/exams)

echo '
	<form action="/cgi-bin/admin/exam_accounts_view_reset_passwords.cgi" method="post">
		<button formaction="/cgi-bin/admin/exam_accounts.cgi" class="button">
			'$"Manage Exam Accounts"'
		</button>
	</form>

	<form action="/cgi-bin/admin/exam_accounts_copy_data.cgi" method="post">
 	<table>
    	<tbody>
		<tr>
			<td class="karoshi-input">'$"Number of Exam Accounts"'</td>
			<td></td>
			<td>'"$ExamCount"'</td>
			<td></td>
		</tr>
		<tr>
			<td class="karoshi-input">'$"Start Account"'</td>
			<td style="width: 60px;">'$"exam"'</td>
			<td><input required class="karoshi-input" name="_EXAMSTART_" maxlength="3" size="3" value="1" type="number"></td>
			<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Choose the start number for the exam account you want to copy the data to. The files will be copied to all of the exam accounts in the range given."'</span></a></td>
		</tr>
		<tr>
			<td>'$"End Account"'</td>
			<td>'$"exam"'</td>
			<td><input required class="karoshi-input" name="_EXAMEND_" maxlength="3" size="3" value="'"$ExamCount"'" type="number"></td>
			<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Choose the end number for the exam account you want to copy the data to."'</span></a></td>
		</tr>
		<tr>
			<td>'$"Exceptions"'</td>
			<td></td>
			<td><input required class="karoshi-input" name="_EXCEPTIONLIST_"  type="text"></td>
			<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Enter in any exam accounts that you do not want the files to be copied to separated by spaces."'</span></a></td>
		</tr>


		<tr>
			<td>'$"Read only"'</td>
			<td></td>
			<td><input id="ReadOnly" name="_READONLY_" value="readonly" type="checkbox"><label for="ReadOnly"> </label></td>
			<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"This will set the exam_files folder and the uploaded files to read only in each account. Students will need to copy the files out of the folder to use them."'</span></a></td>
		</tr>
	</tbody>
	</table>
	<br>
	<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
	</form>
	</display-karoshicontent>
</body>
</html>
'

exit

