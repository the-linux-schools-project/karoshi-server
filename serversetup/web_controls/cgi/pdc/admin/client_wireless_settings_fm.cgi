#!/bin/bash
#Copyright (C) 2013  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Client Wireless Settings"
TITLEHELP=$"This will save the wifi information on the netlogon share for your clients to use when accessing wifi access points."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Client_Wireless_Settings"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "
<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
FILE=$(echo "$DATA" | cut -s -d_ -f3)

echo '<form name="myform" action="/cgi-bin/admin/client_wireless_settings.cgi" method="post">'

[ -f /var/lib/samba/netlogon/domain_information/wifi_ssid ] && SSID=$(sed -n 1,1p /var/lib/samba/netlogon/domain_information/wifi_ssid)
[ -f /var/lib/samba/netlogon/domain_information/wifi_key ] && KEY=$(sed -n 1,1p /var/lib/samba/netlogon/domain_information/wifi_key)

echo '
<table>
	<tbody>
		<tr>
			<td class="karoshi-input">'$"Wifi - SSID"'</td>
			<td><input tabindex= "1" value="'"$SSID"'" name="_SSID_" class="karoshi-input" size="20" type="text"></td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Client_Wireless_Settings"><span class="icon-large-tooltip">'$"Enter in the SSID name for your wireless access points."'</span></a></td>
		</tr>
		<tr>
			<td>'$"Wifi key"'</td>
			<td><input tabindex= "2" value="'"$KEY"'" name="_KEY_" class="karoshi-input" size="20" type="text"></td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Client_Wireless_Settings"><span class="icon-large-tooltip">'$"Enter in the wifi key for your wireless access points."'</span></a></td>
		</tr>
	</tbody>
</table>
<br>
<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</form>
</display-karoshicontent>
</body>
</html>'
exit

