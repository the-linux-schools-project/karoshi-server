#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk
#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

source /opt/karoshi/web_controls/version
source /opt/karoshi/server_network/domain_information/domain_name

TITLE=$"About Karoshi"
TITLEHELP=$"Information about your Karoshi installation."
HELPURL="http://www.linuxschools.com"

#Get the Karoshi version for the server
VERSIONINFO=$(lsb_release -a 2>/dev/null)
KAROSHIVER=$(/opt/karoshi/web_controls/get_version "$VERSIONINFO")

#Get install type
INSTALL_TYPE=education
if [ -f /opt/karoshi/server_network/install_type ]
then
	INSTALL_TYPE=$(sed -n 1,1p /opt/karoshi/server_network/install_type)
fi

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<table>
	<tbody>
		<tr>
			<td class="karoshi-input">'$"Karoshi Version"'</td>
			<td>'"$KAROSHIVER"'</td>
		</tr>
		<tr>
			<td>'$"Web Management Version"'</td>
			<td>'"$VERSION"'</td>
		</tr>
		<tr>
			<td>'$"Licence"'</td>
			<td>GNU AFFERO GENERAL PUBLIC LICENSE Version 3</td>
		</tr>
		<tr>
			<td>'$"Realm"'</td>
			<td>'"$REALM"'</td>
		</tr>



	</tbody>
</table>
</display-karoshicontent>
</body>
</html>'
exit

