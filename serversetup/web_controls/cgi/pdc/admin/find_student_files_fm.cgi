#!/bin/bash
#Change password
#Copyright (C) 2011  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Find student files and folders"
TITLEHELP=$"This will search students home areas for files and folders."
HELPURL="#"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '<div id="suggestions"></div>
<table>
	<tbody>
		<tr>
			<td class="karoshi-input"><b>'$"File / folder name"'</b></td>
			<td><input tabindex= "1" class="karoshi-input" name="_FILENAME_" value="'$FILENAME'" size="20" type="text"></td>
			<td><a class="info" href="javascript:void(0)"><img class="images" alt="" src="/images/help/info.png"><span>'$"Enter in the name of the file or folder you want to search for."'</span></a>
</td>
		</tr>
		<tr>
			<td><b>'$"Find"'</b></td>
			<td></td>
			<td style="text-align: center;"><input id="FindFiles" checked="checked" name="_OPTION_" value="find" type="radio"><label for="FindFiles"> </label></td>
		</tr>
		<tr>
			<td><b>'$"Delete"'</b></td>
			<td><span style="color: red;"><b>'$"Use with extreme care"'</b></span></td>
			<td><input id="DeleteFiles" name="_OPTION_" value="delete" type="radio"><label for="DeleteFiles"> </label></td>
		</tr>
	</tbody>
</table>
'

#Show list of servers
/opt/karoshi/web_controls/show_servers $MOBILE servers $"Search server"

echo '
</form>
</display-karoshicontent>
</body>
</html>'
exit
