#!/bin/bash
#Copyright (C) 2010  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Custom Application"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:%\-+')
#########################
#Assign data to variables
#########################
END_POINT=5
#Assign SERVERNAME

COUNTER=2
while [ "$COUNTER" -le "$END_POINT" ]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [ $(echo "$DATAHEADER"'check') = SERVERNAMEcheck ]
	then
		let COUNTER="$COUNTER"+1
		SERVERNAME=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER="$COUNTER"+1
done

[ ! -z  "$SERVERNAME" ] && TITLE="$TITLE - $SERVERNAME"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/karoshi_servers_view.cgi"
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#########################
#Check data
#########################
#Check to see that servername is not blank
if [ -z "$SERVERNAME" ]
then
	MESSAGE=$"The server cannot be blank."
	show_status
fi

echo '<form action="/cgi-bin/admin/module_custom.cgi" method="post">
<b>'$"Description"'</b><br><br>
'$"This will add in the name of a custom role that you have added to this server. The only purpose of this feature is to record the custom role on the show servers page in the web management."'
<input name="_SERVERNAME_" value="'"$SERVERNAME"'" type="hidden">
<br><br><b>'$"Parameters"'</b><br><br>
<table>
	<tbody>
		<tr>
			<td class="karoshi-input">'$"Custom Application"'</td>
			<td><input class="karoshi-input" tabindex= "2" name="_CUSTOM_" size="20" type="text"></td>
		</tr>
	</tbody>
</table>
<input value="'$"Submit"'" class="button primary" type="submit">
</form>
</display-karoshicontent>
</body>
</html>
'
exit
