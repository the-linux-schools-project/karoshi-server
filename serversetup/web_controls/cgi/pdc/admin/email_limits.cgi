#!/bin/bash
#Copyright (C) 2007 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"E-Mail Limits"
TITLEHELP=$"E-Mail size and queue limits."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=E-Mail_Limits"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

############################
#Show page
############################
function show_status {
echo '
<script>
alert("'"$MESSAGE"'");
</script>
</display-karoshicontent>
</body>
</html>'
exit
}
#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')

#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi

echo '<form action="/cgi-bin/admin/email_limits2.cgi" method="post">'

#Get current email settings
Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/email_limits.cgi | cut -d' ' -f1)
sudo -H /opt/karoshi/web_controls/exec/email_limits_view "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$MOBILE:"
echo '
<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</display-karoshicontent>
</form>
</body>
</html>'
exit
