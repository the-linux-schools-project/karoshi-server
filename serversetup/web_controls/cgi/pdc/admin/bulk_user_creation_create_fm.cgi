#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Bulk User Creation"
TITLEHELP=$"Choose the username style and primary group for the users you want to create."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Upload_CSV#Username_Styles_and_Primary_Group"

#Select the default username style
if [ -f /opt/karoshi/server_network/default_username_style ]
then
	source /opt/karoshi/server_network/default_username_style
	[ "$DEFAULTSTYLE" = 1 ] && SELECT1='selected="selected"'
	[ "$DEFAULTSTYLE" = 2 ] && SELECT2='selected="selected"'
	[ "$DEFAULTSTYLE" = 3 ] && SELECT3='selected="selected"'
	[ "$DEFAULTSTYLE" = 4 ] && SELECT4='selected="selected"'
	[ "$DEFAULTSTYLE" = 5 ] && SELECT5='selected="selected"'
	[ "$DEFAULTSTYLE" = 6 ] && SELECT6='selected="selected"'
	[ "$DEFAULTSTYLE" = 7 ] && SELECT7='selected="selected"'
	[ "$DEFAULTSTYLE" = 8 ] && SELECT8='selected="selected"'
	[ "$DEFAULTSTYLE" = 9 ] && SELECT9='selected="selected"'
	[ "$DEFAULTSTYLE" = 10 ] && SELECT10='selected="selected"'
else
	SELECT1='selected="selected"'
fi

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin


#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<form action="/cgi-bin/admin/bulk_user_creation_create.cgi" method="post">
	<button class="button" formaction="bulk_user_creation_upload_fm.cgi" name="_BulkUserCreation_" value="_">
		'$"Bulk User Creation"'
	</button>

	<button class="button" formaction="bulk_user_creation_import_enrollment_numbers_fm.cgi" name="_ImportEnrollmentNumbers_" value="_">
		'$"Import Enrollment Numbers"'
	</button>
	<br><br>

	'$"Select the following options to create your users"':<br><br>'

#Check that this server is not part of a federated setup
if [ -f /opt/karoshi/server_network/servers/"$HOSTNAME"/federated_server ]
then
	echo $"This server is part of a federated system. Users must be created on the main federation server." '</div></div></body></html>'
	exit
fi

echo '<table class="standard" style="text-align: left;" >
    <tbody>
      <tr>
        <td style="width: 150px;">
'$"Username"'</td><td>
<select name="_USERNAMESTYLE_" class="karoshi-input">
        <option value="userstyleS1" '"$SELECT1"'>'$"Style"' 1: '$"auser09"'</option>
        <option value="userstyleS2" '"$SELECT2"'>'$"Style"' 2: '$"09auser"'</option>
        <option value="userstyleS3" '"$SELECT3"'>'$"Style"' 3: '$"usera09"'</option>
        <option value="userstyleS4" '"$SELECT4"'>'$"Style"' 4: '$"arnold.user09"'</option>
        <option value="userstyleS5" '"$SELECT5"'>'$"Style"' 5: '$"user.arnold09"'</option>
        <option value="userstyleS6" '"$SELECT6"'>'$"Style"' 6: '$"09usera"'</option>
        <option value="userstyleS7" '"$SELECT7"'>'$"Style"' 7: '$"09arnoldu"'</option>
        <option value="userstyleS8" '"$SELECT8"'>'$"Style"' 8: '$"arnoldu"'</option>
        <option value="userstyleS9" '"$SELECT9"'>'$"Style"' 9: '$"Enrollment number as username"'</option>
	<option value="userstyleS10" '"$SELECT10"'>'$"Style"' 10: '$"Enter a username"'</option>
</select></td></tr>
<tr><td>'$"Primary Group"'</td><td>'
/opt/karoshi/web_controls/group_dropdown_list | sed 's/.*<option disabled selected value>.*/<option label="blank"><\/option><option value="getgroupfromcsv">'$"Primary group from CSV"'<\/option><option disabled value>-----------------<\/option>/g'
echo '</td></tr></tbody></table>
<br><br>
 <input value="'$"Submit"'" class="button" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</form>
</display-karoshicontent>
</body>
</html>
'
exit

