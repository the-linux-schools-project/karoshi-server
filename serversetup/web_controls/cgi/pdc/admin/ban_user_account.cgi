#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Ban User Account"
BANLENGTH=7

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#########################
#Get data input
#########################
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
if [ ! -z "$DATA" ]
then
	END_POINT=18
	#Assign HOUR
	DATANAME=HOUR
	get_data
	HOUR=$(echo "$DATAENTRY" | sed 's/3F/?/g')

	#Assign MINUTES
	DATANAME=MINUTES
	get_data
	MINUTES=$(echo "$DATAENTRY" | sed 's/3F/?/g')

	#Assign DAY
	DATANAME=DAY
	get_data
	DAY=$(echo "$DATAENTRY" | sed 's/3F/?/g')

	#Assign MONTH
	DATANAME=MONTH
	get_data
	MONTH=$(echo "$DATAENTRY" | sed 's/3F/?/g')

	#Assign YEAR
	DATANAME=YEAR
	get_data
	YEAR=$(echo "$DATAENTRY" | sed 's/3F/?/g')

	#Assign INCIDENT
	DATANAME=INCIDENT
	get_data
	INCIDENT=$(echo "$DATAENTRY" | sed 's/2B/ /g')

	#Assign ACTIONTAKEN
	DATANAME=ACTIONTAKEN
	get_data
	ACTIONTAKEN=$(echo "$DATAENTRY" | sed 's/2B/ /g')

	#Assign STUDENTS
	DATANAME=STUDENTS
	get_data
	STUDENTS=$(echo "$DATAENTRY" | sed 's/2B/ /g')

	#Assign BANLENGTH
	DATANAME=BANLENGTH
	get_data
	BANLENGTH=$(echo "$DATAENTRY" | sed 's/2B/ /g')
fi
DATE_INFO=$(date +%F)
[ -z "$DAY" ] && DAY=$(echo "$DATE_INFO" | cut -d- -f3)
[ -z "$MONTH" ] && MONTH=$(echo "$DATE_INFO" | cut -d- -f2)
[ -z "$YEAR" ] && YEAR=$(echo "$DATE_INFO" | cut -d- -f1)

TIME_INFO=$(date +%T)
[ -z "$HOUR" ] && HOUR=$(echo "$TIME_INFO" | cut -d: -f1)
[ -z "$MINUTES" ] && MINUTES=$(echo "$TIME_INFO" | cut -d: -f2)

function show_status {
echo '<SCRIPT language="Javascript">'
echo 'alert("'"$MESSAGE"'")';
echo '                window.location = "/cgi-bin/admin/ban_user_account.cgi";'
echo '</script>'
echo "</div></body></html>"
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
	<form action="banned_users_view_fm.cgi" method="post">
		<button class="button" name="_ViewBannedUsers_" value="_">
			'$"Banned Users"'
		</button>
	</form>

	<form action="/cgi-bin/admin/ban_user_account2.cgi" method="post">
		<table>
			<tbody>
				<tr>
					<td class="karoshi-input">'$"Time"'</td>
					<td>
						<input style="width: 70px; display: inline;" name="_HOUR_" value="'"$HOUR"'" size="2" maxlength="2" type="text">
						:
						<input style="width: 70px; display: inline;" name="_MINUTES_" value="'"$MINUTES"'" size="2" maxlength="2" type="text"></td>
					<td></td>
				</tr>
				<tr>
					<td class="karoshi-input">'$"Day"'</td>
					<td><input class="karoshi-input" name="_DAY_" value="'"$DAY"'" size="2" maxlength="2" type="text"></td>
					<td></td>
				</tr>
				<tr>
					<td class="karoshi-input">'$"Month"'</td>
					<td><input class="karoshi-input" name="_MONTH_" value="'"$MONTH"'" size="2" maxlength="2" type="text"></td>
					<td></td>
				</tr>
				<tr>
					<td class="karoshi-input">'$"Year"'</td>
					<td><input class="karoshi-input" name="_YEAR_" value="'"$YEAR"'" size="4" maxlength="4" type="text"></td>
					<td></td>
				</tr>
				<tr>
					<td>'$"Ban duration in days"'</td>
					<td><input class="karoshi-input" name="_BANLENGTH_" value="'"$BANLENGTH"'" size="2" maxlength="3" type="number"></td>
					<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Acceptable_Use"><span class="icon-large-tooltip">'$"Leave blank for a permanent ban."' '$"Users are automatically allowed after their ban duration is up. The ban duration will include weekends and holidays."'</span></a>
					</td>
				</tr>
			</tbody>
		</table>'
#Users involved
echo '
		<table>
			<tbody>
				<tr>
					<td>'$"Enter the usernames you want to ban from the system separated by spaces:"'</td>
				</tr>
				<tr>
					<td><input required="required" value="'"$STUDENTS"'" name="_STUDENTS_" size="78" type="text"></td>
				</tr>
				<tr>
					<td>Incident Report</td>
				</tr>
				<tr>
					<td><textarea cols="90" rows="4" name="_INCIDENT_">'"$INCIDENT"'</textarea></td>
				</tr>
				<tr>
					<td>Action Taken</td>
				</tr>
				<tr>
					<td><textarea cols="90" rows="4" name="_ACTIONTAKEN_">'$"User account banned."'</textarea></td>
				</tr>
			</tbody>
		</table>
		<input value="Submit" class="button primary" type="submit"> <input value="Reset" class="button" type="reset">
	</form>
</display-karoshicontent>
</body></html>'
exit

