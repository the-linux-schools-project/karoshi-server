#!/bin/bash
#Copyright (C) 2011  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Add a device"
TITLEHELP=$"This will add a device to the ups list to show that it is phsyically plugged into the ups. It will not configure the device."'<br><br>'$"This could be a network switch or a non Karoshi server."
HELPURL="#"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')

END_POINT=9
#Assign UPSSERVER
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = UPSSERVERcheck ]
	then
		let COUNTER=$COUNTER+1
		UPSSERVER=`echo $DATA | cut -s -d'_' -f$COUNTER`
		break
	fi
	let COUNTER=$COUNTER+1
done

#Get current date and time
DAY=`date +%d`
MONTH=`date +%m`
YEAR=`date +%Y`

HOUR=`date +%H`
MINUTES=`date +%M`
SECONDS=`date +%S`

function show_status {
echo '<SCRIPT language="Javascript">'
echo 'alert("'$MESSAGE'")';
echo 'window.location = "/cgi-bin/admin/ups_add_fm.cgi";'
echo '</script>'
echo "</div></body></html>"
exit
}

#########################
#Check https access
#########################
if [ https_$HTTPS != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER": /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

#Check to see if there are any master ups available

if [ ! -d /opt/karoshi/server_network/ups/master ]
then
	MESSAGE=$"There are no master UPS devices available."
	show_status
fi

if [[ $(ls -1 /opt/karoshi/server_network/ups/master/ | wc -l) = 0 ]]
then
	MESSAGE=$"There are no master UPS devices available."
	show_status
fi

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<form action="/cgi-bin/admin/ups_status.cgi" name="Status" method="post">
	<button class="button" name="_UPSStatus" value="_">
		'$"UPS Status"'
	</button>
</form>
<form action="/cgi-bin/admin/ups_device_add.cgi" name="tstest" method="post">
	<table>
		<tbody>
			<tr>
				<td class="karoshi-input">'$"Master UPS"'</td>
				<td>'

if [ -z "$UPSSERVER" ]
then
	#Generate list of UPC data
	echo '
				<select name="_UPSSERVER_" class="karoshi-input">
					<option> </option>'

	for UPSSERVERS in /opt/karoshi/server_network/ups/master/*
	do
		=`basename $UPSSERVERS`
		if [ -d /opt/karoshi/server_network/ups/master/$UPSSERVER/drivers/ ]
		then
			if [ `ls -1 /opt/karoshi/server_network/ups/master/$UPSSERVER/drivers/ | wc -l` != 0 ]
			then
				for UPSMODELS in /opt/karoshi/server_network/ups/master/$UPSSERVER/drivers/*
				do
					UPSMODEL=`basename $UPSMODELS`
					echo '
					<option value="'$UPSSERVER','$UPSMODEL'">'$UPSSERVER': '$UPSMODEL'</option>'
				done
			fi
		fi
	done
	echo '
				</select>'
else
	echo '
				<input type="hidden" name="_UPSSERVER_" value="'$UPSSERVER'">'$UPSSERVER''
fi
echo '
				</td>'
[ -z "$UPSSERVER" ] && echo '
				<td><a class="info" href="javascript:void(0)"><img class="images" alt="" src="/images/help/info.png"><span>'$"Choose a master UPS device from the list."'</span></a></td>'
echo '
			</tr>
			<tr>
				<td>'$"Device"'</td><td><input tabindex= "2" name="_DEVICENAME_" class="karoshi-input" size="20" type="text"></td>
			</tr>
		</tbody>
	</table>
	<br>
	<br>
	<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</form>
</display-karoshicontent>
</body>
</html>'
exit



