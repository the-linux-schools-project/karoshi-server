#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

#Detect mobile browser
MOBILE=no
source /opt/karoshi/web_controls/detect_mobile_browser
source /opt/karoshi/web_controls/version

TITLE=$"Custom Command"
TITLEHELP=$"Please use this feature with care."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Custom_Commands"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '
<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "
		<display-karoshicontent>
		<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
		<form action="/cgi-bin/admin/custom_command.cgi" name="selectservers" method="post"><b></b>
		<table>
			<tbody>
				<tr>
					<td class="karoshi-input">'$"Command"'</td>
					<td><input required="required" name="_CUSTOMCOMMAND_" size="60" type="text"></td>
					<td><a class="info icon icon-large fa-info-circle" target="_blank" href="'"$HELPURL"'"><span class="icon-large-tooltip">'$"Allowed special keys /+ -"'</span></a></td>
				</tr>
			</tbody>
		</table><br>
'

#Show list of servers
/opt/karoshi/web_controls/show_servers $MOBILE servers $"Run Command"

echo '
		</form>
		</display-karoshicontent>
	</body>
</html>'

exit
