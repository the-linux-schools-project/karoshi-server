#!/bin/bash
#Copyright (C) 2016 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Quick Links"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Detect mobile browser
MOBILE=no
source /opt/karoshi/web_controls/detect_mobile_browser

#########################
#Get data input
#########################

DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-%+-' | sed 's/____/QUADRUPLEUNDERSCORE/g' | sed 's/_/REPLACEUNDERSCORE/g' | sed 's/QUADRUPLEUNDERSCORE/_/g')

function show_status {
echo '
<script>
alert("'"$MESSAGE"'");
window.location = "/cgi-bin/admin/mylinks.cgi"
</script>
</form></display-karoshicontent></body></html>'
exit
}

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Assign data to variables
#########################
END_POINT=15
MAX_SUB_LINKS=20
MAX_INLINE_LINKS=5
MAX_QUICK_LINKS=200
#Assign ACTION
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = ACTIONcheck ]
	then
		let COUNTER=$COUNTER+1
		ACTION=`echo $DATA | cut -s -d'_' -f$COUNTER | sed 's/REPLACEUNDERSCORE/_/g'`
		break
	fi
	let COUNTER=$COUNTER+1
done

[ -z "$ACTION" ] && ACTION=view

if [ "$ACTION" = add ] || [ "$ACTION" = delete ] || [ "$ACTION" = up ] || [ "$ACTION" = down ]
then
	#Assign HYPERLINK
	COUNTER=2
	while [ $COUNTER -le $END_POINT ]
	do
		DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
		if [ `echo $DATAHEADER'check'` = HYPERLINKcheck ]
		then
			let COUNTER=$COUNTER+1
			HYPERLINK=`echo $DATA | cut -s -d'_' -f$COUNTER`
			break
		fi
		let COUNTER=$COUNTER+1
	done

	#Assign QUICKLINKSTYLE
	COUNTER=2
	while [ $COUNTER -le $END_POINT ]
	do
		DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
		if [ `echo $DATAHEADER'check'` = QUICKLINKSTYLEcheck ]
		then
			let COUNTER=$COUNTER+1
			QUICKLINKSTYLE=`echo $DATA | cut -s -d'_' -f$COUNTER`
			break
		fi
		let COUNTER=$COUNTER+1
	done
fi

#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [ `grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin` != 1 ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

#########################
#Check data
#########################

#Check to see that GROUPNAME is not blank
TITLE=$"Quick Links"

if [ "$ACTION" != add ] && [ "$ACTION" != delete ] && [ "$ACTION" != view ] && [ "$ACTION" != up ] && [ "$ACTION" != down ]
then
	MESSAGE=$"An incorrect action has been entered."
	show_status
fi

if [ "$ACTION" = add ] || [ $ACTION = delete ] || [ "$ACTION" = UP ] || [ "$ACTION" = down ]
then
	#Check to see that HYPERLINK is not blank
	if [ -z "$HYPERLINK" ]
	then
		MESSAGE=$"The hyperlink cannot be blank."
		show_status
	fi
fi

if [ "$ACTION" = add ]
then
	if [ -f "/opt/karoshi/web_controls/user_prefs/$REMOTE_USER.links.$QUICKLINKSTYLE" ]
	then
		#Check how many links we already have
		if [ $QUICKLINKSTYLE = sub ]
		then
			MAX_LINKS=$MAX_SUB_LINKS
		elif [ $QUICKLINKSTYLE = inline ]
		then
			MAX_LINKS=$MAX_INLINE_LINKS
		else
			MAX_LINKS=$MAX_QUICK_LINKS
		fi
		LINK_COUNT=$(cat "/opt/karoshi/web_controls/user_prefs/$REMOTE_USER.links.$QUICKLINKSTYLE" | wc -l)
		if [ $LINK_COUNT -ge $MAX_LINKS ]
		then
			MESSAGE=$"The maximum links for this section has been reached."
			show_status	
		fi
	fi
fi

if [ "$ACTION" = add ] || [ "$ACTION" = delete ] || [ "$ACTION" = up ] || [ "$ACTION" = down ]
then
	Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/mylinks.cgi | cut -d' ' -f1)
	echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$ACTION:$HYPERLINK:$QUICKLINKSTYLE:" | sudo -H /opt/karoshi/web_controls/exec/mylinks
	#Reload page
	echo '
	<script>
		window.location = "mylinks.cgi";
	</script>
	</display-karoshicontent>
	</body>
	</html>
	'
fi

#Generate navigation bar
if [ "$MOBILE" = no ]
then
	TABLECLASS=standard
	WIDTH1=200
	WIDTH2=210
	WIDTH3=130
	WIDTH4=375
	WIDTH5=80
	HEIGHT1=25
	ICON1=/images/submenus/file/moveup.png
	ICON2=/images/submenus/file/movedown.png
	ICON3=/images/submenus/file/delete.png
else
	TABLECLASS=mobilestandard
	WIDTH1=80
	WIDTH2=210
	WIDTH3=80
	WIDTH4=200
	WIDTH5=40
	HEIGHT1=30
	ICON1=/images/submenus/file/moveupm.png
	ICON2=/images/submenus/file/movedownm.png
	ICON3=/images/submenus/file/deletem.png
fi

#Show box to add link
echo '
<form name="myform" action="/cgi-bin/admin/mylinks.cgi" method="post">
	<table id="myTable" class="tablesorter" style="text-align: left;">
		<tbody>
			<tr>
				<td class="karoshi-input">'$"Link Style"'</td>
				<td>
					<select name="____QUICKLINKSTYLE" class="karoshi-input">
						<option value="____sub____">'$"Sub Link"'</option>
						<option value="____inline____">'$"Main Link"'</option>
						<option value="____quick____">'$"Bookmark"'</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>'$"Add link"'</td>
				<td>'
#Show a drop down of all available links
echo '
				<select name="____ACTION____add________HYPERLINK____" class="karoshi-input">'
/opt/karoshi/web_controls/generate_sidebar_admin | sed 's/\t//g' | grep '^<li><a href="'  | sed 's/<li>//g' | sed 's/<\/li>//g' | sed 's$<a href=$<option value=$g' | sed 's$</a>$</option>$g' | grep -v "<span" | cut -d "<" -f1-3 | sort --unique
echo '
					</select>
				</td>
			</tr>
		</tbody>
	</table>
	<input value="'$"Submit"'" class="button primary" type="submit">
</form>
'

function show_current_links {
#Show current sub links
if [ -f /opt/karoshi/web_controls/user_prefs/"$REMOTE_USER".links.$QUICKLINKSTYLE ]
then
	if [ "$QUICKLINKSTYLE" = sub ]
	then
		MAX_LINKS="$MAX_SUB_LINKS"
	elif [ "$QUICKLINKSTYLE" = inline ]
	then
		MAX_LINKS="$MAX_INLINE_LINKS"
	else
		MAX_LINKS="$MAX_QUICK_LINKS"
	fi
	LINK_COUNT=$(cat "/opt/karoshi/web_controls/user_prefs/$REMOTE_USER.links.$QUICKLINKSTYLE" | wc -l)

	COUNTER=1
	echo '<a name="'"$QUICKLINKSTYLE"'"></a>
<form name="myform" action="/cgi-bin/admin/mylinks.cgi#'"$QUICKLINKSTYLE"'" method="post"> <input type="hidden" name="____Quicklinks____" value="____QUICKLINKSTYLE____'"$QUICKLINKSTYLE"'____">
	<table class="tablesorter" style="text-align: left;">
		<thead>
			<tr>
				<th style="width: '$WIDTH4'px;">'"$QUICKLNKTITLE"' '"$LINK_COUNT"'/'"$MAX_LINKS"'</th>
				<th style="width: '"$WIDTH5"'px;text-align:center">'$"Move"'</th><th style="width: '"$WIDTH5"'px; text-align:center">'$"Delete"'</th>
			</tr>
		</thead>
		<tbody>'
	LINKCOUNT=$(cat /opt/karoshi/web_controls/user_prefs/"$REMOTE_USER.links.$QUICKLINKSTYLE" | wc -l)
	for LINKDATA in $(cat /opt/karoshi/web_controls/user_prefs/"$REMOTE_USER.links.$QUICKLINKSTYLE" | sed 's% %SPACE%g')
	do
		HYPERLINK=$(echo "$LINKDATA" | cut -d, -f1)
		LINKTITLE=$(echo "$LINKDATA" | cut -d, -f2 | sed 's%SPACE% %g')
		echo '
			<tr>
				<td><a href="'"$HYPERLINK"'">'"$LINKTITLE"'</a></td>
				<td style="text-align:center">'
		if [ $COUNTER != 1 ]
		then
			echo '
					<button class="button-no-border icon icon-large fa-chevron-circle-up" name="____Up____" value="____ACTION____up____HYPERLINK____'"$HYPERLINK"'____">
					</button>'
		fi
		if [ "$COUNTER" != "$LINKCOUNT" ]
		then
			echo '
					<button class="button-no-border icon icon-large fa-chevron-circle-down" name="____Down____" value="____ACTION____down____HYPERLINK____'"$HYPERLINK"'____">
					</button>'
		fi

		echo '
				</td>
				<td style="text-align:center">
					<button class="info" name="____Delete____" value="____ACTION____delete____HYPERLINK____'"$HYPERLINK"'____">
						<img src="'"$ICON3"'" alt="'$"Delete"'">
						<span>'$"Delete"'<br>'"$LINKTITLE"'</span>
					</button>
				</td>
			</tr>'
		let COUNTER="$COUNTER"+1
	done
	echo '
		</tbody>
	</table>
</form>'
fi
}

QUICKLNKTITLE=$"Bookmarks"
QUICKLINKSTYLE=quick
show_current_links

QUICKLNKTITLE=$"Main Links"
QUICKLINKSTYLE=inline
show_current_links

QUICKLNKTITLE=$"Sub Links"
QUICKLINKSTYLE=sub
show_current_links

echo '</display-karoshicontent></body></html>'

exit

