#!/bin/bash
#Copyright (C) 2009  Paul Sharrad
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Request Delete Users"
TITLEHELP=$"This will forward a request to the network manager for users to be deleted with the details that you provide."
HELPURL="#"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_staff

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<form action="/cgi-bin/staff/request_delete_users.cgi" method="post">
<table>
	<thead>
		<tr>
			<th>'$"Forename"'</th>
			<th>'$"Surname"'</th>
			<th>'$"Primary Group"' <a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Enter in the year group if this is a student or the role if it is a member of staff."'</span></a></th>
			<th>'$"Admission Number"' <a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Enter in the admission number for a student or leave blank for a member of staff."'</span></a></th>
		</tr>
	</thead>
	<tbody>'

for i in $(seq 1 10)
do
        echo '
		<tr>
			<td><input style="width: 200px;" tabindex= "0" size="10" name="_FORENAME'"$i"'_" type="text"></td>
			<td><input style="width: 200px;" tabindex= "0" size="15" name="_SURNAME'"$i"'_" type="text"></td><td>'$(/opt/karoshi/web_controls/group_dropdown_list | sed "s/GROUP/GROUP$i/g")'</td><td><input style="width: 200px;" tabindex= "0" name="_ADNO'"$i"'_" type="text"></td>
		</tr>'
done   

echo '
	</tbody>
</table>
<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</form>
</display-karoshicontent>
</body>
</html>'
exit

