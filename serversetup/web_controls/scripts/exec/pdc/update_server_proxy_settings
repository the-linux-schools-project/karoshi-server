#!/bin/bash
#Copyright (C) 2011 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk
LOG_DATE=$(date +%F)
[ -z "$LOG_DATE" ] && exit
########################
#Check md5checksum
########################
if ! test -f /opt/karoshi/web_controls/checksums/admin_checksums/update_server_proxy_settings_cgi
then
	echo "$(date): update_server_proxy_settings - No Checksum" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
source /opt/karoshi/web_controls/checksums/admin_checksums/update_server_proxy_settings_cgi
Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/update_server_proxy_settings.cgi | cut -d' ' -f1)
[ -z "$Checksum" ] && Checksum=not_set
if [ "$Checksum"'check' != "$update_server_proxy_settings_cgi"'check' ]
then
	echo "$(date): update_server_proxy_settings - Incorrect Checksum" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi

########################
#Get variables
########################
numArgs="$#"
if [ "$numArgs" != 0 ]
then
	echo "$(date): update_server_proxy_settings - incorrect number of arguments" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi

read DATA
DATA=$(echo "$DATA" | tr -cd 'A-Za-z0-9\._:%\n-+-')
if [ -z "$DATA" ]
then
	echo "$(date): update_server_proxy_settings - no data" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
REMOTE_USER=$(echo "$DATA" | cut -s -d: -f1)
REMOTE_ADDR=$(echo "$DATA" | cut -s -d: -f2)
RemoteCheckSum=$(echo "$DATA" | cut -s -d: -f3)
SERVERNAME=$(echo "$DATA" | cut -s -d: -f4)
SERVERTYPE=$(echo "$DATA" | cut -s -d: -f5)
SERVERMASTER=$(echo "$DATA" | cut -s -d: -f6)
TCPIP=$(echo "$DATA" | cut -s -d: -f7)
PORT=$(echo "$DATA" | cut -s -d: -f8)
USERNAME=$(echo "$DATA" | cut -s -d: -f9)
PASSWORD=$(echo "$DATA" | cut -s -d: -f10)
########################
#Check data
########################
if [ "$RemoteCheckSum"'check' != "$Checksum"'check' ] && [ "$RemoteCheckSum"'check' != "$Checksum2"'check' ]
then
	echo "$(date): update_server_proxy_settings - Not called by update_server_proxy_settings.cgi" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
if [ -z "$REMOTE_USER" ]
then
	echo "$(date): update_server_proxy_settings - Blank remote user" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
if [ -z "$REMOTE_ADDR" ]
then
	echo "$(date): update_server_proxy_settings - Blank remote tcpip address" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi

if [[ $(grep -c ^"$REMOTE_USER": /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	echo "$(date): update_server_proxy_settings - access denied to $REMOTE_USER from $REMOTE_ADDR" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi

echo "$(date): update_server_proxy_settings configured for $SERVERNAME by $REMOTE_USER from $REMOTE_ADDR" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"

if [ ! -d /opt/karoshi/server_network/upstream_proxy_settings ]
then
	mkdir -p /opt/karoshi/server_network/upstream_proxy_settings
	chmod 0750 /opt/karoshi/server_network/upstream_proxy_settings
	chown root.apache_karoshi /opt/karoshi/server_network/upstream_proxy_settings
fi

echo -e 'TCPIP="'"$TCPIP"'"
PORT="'"$PORT"'"
USERNAME="'"$USERNAME"'"
PASSWORD="'"$PASSWORD"'"' > /opt/karoshi/server_network/upstream_proxy_settings/"$SERVERNAME"

DIRECT_ACCESS=no
if [ -z "$PORT" ] || [ -z "$TCPIP" ]
then
	DIRECT_ACCESS=yes
fi

USE_AUTH=yes
if [ -z "$USERNAME" ] || [ -z "$PASSWORD" ]
then
	USE_AUTH=no
fi

if [[ "$SERVERNAME" = $(hostname-fqdn) ]]
then
	########################
	#Configure wget to use proxy server
	########################
	#Create wgetrc
	[ -f "$HOME"/.wgetrc ] && rm -f "$HOME"/.wgetrc
	touch "$HOME"/.wgetrc
	chmod 0600 "$HOME"/.wgetrc

	if [ "$DIRECT_ACCESS" = no ]
	then
		echo 'http_proxy = http://'"$TCPIP:$PORT"'' > "$HOME"/.wgetrc
		echo 'https_proxy = http://'"$TCPIP:$PORT"'' >> "$HOME"/.wgetrc
		echo no_proxy=localhost,127.0.0.1 >> "$HOME"/.wgetrc
		if [ "$USE_AUTH" = yes ]
		then
			echo 'proxy_user = '"$USERNAME"'' >> "$HOME"/.wgetrc
			echo 'proxy_password = '"$PASSWORD"'' >> "$HOME"/.wgetrc
		fi
	fi

	########################
	#Configure apt to use proxy server
	########################
	[ -f /etc/apt/apt.conf.d/02proxy ] && rm -f /etc/apt/apt.conf.d/02proxy
	if [ "$DIRECT_ACCESS" = no ]
	then
		if [ "$USE_AUTH" = yes ]
		then
			echo 'Acquire::http::Proxy "http://'"$USERNAME"':'"$PASSWORD"'@'"$TCPIP"':'"$PORT"'";' > /etc/apt/apt.conf.d/02proxy
		else
			echo 'Acquire::http::Proxy "http://'"$TCPIP"':'"$PORT"'";' > /etc/apt/apt.conf.d/02proxy
		fi
	fi
fi

if [[ "$SERVERNAME" != $(hostname-fqdn) ]]
then
	if [ "$SERVERTYPE" = network ] || [ "$SERVERTYPE" = federated ]
	then
		ssh -o PasswordAuthentication=no -o ConnectTimeout=3 "$SERVERNAME" '
		[ -d /opt/karoshi/logs/karoshi_web_management/ ] && echo "$(date): update_server_proxy_settings (federated mode) configured for '"$SERVERNAME"' by '"$REMOTE_USER"' from '"$REMOTE_ADDR"'" >> /opt/karoshi/logs/karoshi_web_management/'"$LOG_DATE"'

		########################
		#Configure wget to use proxy server
		########################
		#Create wgetrc
		[ -f "$HOME/.wgetrc" ] && rm -f "$HOME/.wgetrc"
		touch "$HOME/.wgetrc"
		chmod 0600 "$HOME/.wgetrc"

		if [ '"$DIRECT_ACCESS"' = no ]
		then
			echo "http_proxy = http://'"$TCPIP"':'"$PORT"'" > "$HOME/.wgetrc"
			echo "https_proxy = http://'"$TCPIP"':'"$PORT"'" >> "$HOME/.wgetrc"
			echo no_proxy=localhost,127.0.0.1 >> "$HOME/.wgetrc"
			if [ '"$USE_AUTH"' = yes ]
			then
				echo proxy_user = "'"$USERNAME"'" >> "$HOME/.wgetrc"
				echo proxy_password = "'"$PASSWORD"'" >> "$HOME/.wgetrc"
			fi
		fi

		########################
		#Configure apt to use proxy server
		########################
		[ -f /etc/apt/apt.conf.d/02proxy ] && rm -f /etc/apt/apt.conf.d/02proxy
		if [ '"$DIRECT_ACCESS"' = no ]
		then
			if [ '"$USE_AUTH"' = yes ]
			then
				echo "Acquire::http::Proxy \"http://'"$USERNAME"':'"$PASSWORD"'@'"$TCPIP"':'"$PORT"'\";" > /etc/apt/apt.conf.d/02proxy
			else
				echo "Acquire::http::Proxy \"http://'"$TCPIP"':'"$PORT"'\";" > /etc/apt/apt.conf.d/02proxy
			fi
		fi'
	fi
fi

if [[ "$SERVERNAME" != $(hostname-fqdn) ]]
then
	if [ "$SERVERTYPE" = federatedslave ]
	then
		ssh -x -o PasswordAuthentication=no -o ConnectTimeout=3  "$SERVERMASTER" '
		[ -d /opt/karoshi/logs/karoshi_web_management/ ] && echo "$(date): update_server_proxy_settings (federated mode) configured for '"$SERVERNAME"' by '"$REMOTE_USER"' from '"$REMOTE_ADDR"'" >> /opt/karoshi/logs/karoshi_web_management/'"$LOG_DATE"'

		########################
		#Configure wget to use proxy server
		########################
		#Create wgetrc
		[ -f "$HOME/.wgetrc" ] && rm -f "$HOME/.wgetrc"
		touch "$HOME/.wgetrc"
		chmod 0600 "$HOME/.wgetrc"

		if [ '"$DIRECT_ACCESS"' = no ]
		then
			echo "http_proxy = http://'"$TCPIP"':'"$PORT"'" > "$HOME/.wgetrc"
			echo "https_proxy = http://'"$TCPIP"':'"$PORT"'" >> "$HOME/.wgetrc"
			echo no_proxy=localhost,127.0.0.1 >> "$HOME/.wgetrc"
			if [ '"$USE_AUTH"' = yes ]
			then
				echo proxy_user = "'"$USERNAME"'" >> "$HOME/.wgetrc"
				echo proxy_password = "'"$PASSWORD"'" >> "$HOME/.wgetrc"
			fi
		fi

		########################
		#Configure apt to use proxy server
		########################
		[ -f /etc/apt/apt.conf.d/02proxy ] && rm -f /etc/apt/apt.conf.d/02proxy
		if [ '"$DIRECT_ACCESS"' = no ]
		then
			if [ '"$USE_AUTH"' = yes ]
			then
				echo "Acquire::http::Proxy \"http://'"$USERNAME"':'"$PASSWORD"'@'"$TCPIP"':'"$PORT"'\";" > /etc/apt/apt.conf.d/02proxy
			else
				echo "Acquire::http::Proxy \"http://'"$TCPIP"':'"$PORT"'\";" > /etc/apt/apt.conf.d/02proxy
			fi
		fi'
	'\''
	'
	fi
fi

exit

