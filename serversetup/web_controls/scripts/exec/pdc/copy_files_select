#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk
LOG_DATE=$(date +%F)
########################
#Check md5checksum
########################
if ! test -f /opt/karoshi/web_controls/checksums/admin_checksums/copy_files_select_cgi
then
	echo "$(date): copy_files_select - No Checksum" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
source /opt/karoshi/web_controls/checksums/admin_checksums/copy_files_select_cgi
Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/copy_files_select.cgi | cut -d' ' -f1)
[ -z "$Checksum" ] && Checksum=not_set
if [ "$Checksum"'check' != "$copy_files_select_cgi"'check' ]
then
	echo "$(date): copy_files_select - Incorrect Checksum" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
########################
#Rotate event_log
########################
/opt/karoshi/web_controls/rotate_log
########################
#Get variables
########################
numArgs=$#
if [ "$numArgs" != 0 ]
then
	echo "$(date): copy_files_select - incorrect number of arguments" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
read DATA
DATA=$(echo "$DATA" | tr -cd 'A-Za-z0-9\._:%\n-+')
if [ -z "$DATA" ]
then
	echo "$(date): copy_files_select - no data" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
REMOTE_USER=$(echo "$DATA" | cut -s -d: -f1)
REMOTE_ADDR=$(echo "$DATA" | cut -s -d: -f2)
RemoteCheckSum=$(echo "$DATA" | cut -s -d: -f3)
GROUP=$(echo "$DATA" | cut -s -d: -f4)

########################
#Check data
########################
if [ "$RemoteCheckSum"'check' != "$Checksum"'check' ]
then
	echo "$(date): copy_files_select - Not called by copy_files_select.cgi" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
if [ -z "$REMOTE_USER" ]
then
	echo "$(date): copy_files_select - Blank remote user" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
if [ -z "$REMOTE_ADDR" ]
then
	echo "$(date): copy_files_select - Blank remote tcpip address" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
if [[ $(grep -c ^"$REMOTE_USER": /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	echo "$(date): add_user - access denied to $REMOTE_USER from $REMOTE_ADDR" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi
if [ -z "$GROUP" ]
then
	echo "$(date): copy_files_select - incorrect data input by $REMOTE_USER from $REMOTE_ADDR" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
	exit 101
fi


########################
#Copy the data
########################
echo "$(date): copy_files_select - Copying data to group $GROUP by $REMOTE_USER from $REMOTE_ADDR" >> "/opt/karoshi/logs/karoshi_web_management/$LOG_DATE"
source /opt/karoshi/server_network/group_information/"$GROUP"

SOURCEDIR=/var/www/karoshi/web_upload
COPYDIR=$(date +%d-%m-%y).$$

source "/opt/karoshi/server_network/quota_settings/$GROUP"
echo "<ul>"
for User in $(getent group "$GROUP" | cut -d: -f4- | sed 's/,/ /g' )
do
	UserPath=$(getent passwd "$User" | cut -d: -f6)
	echo "<li>"$"Copying files to"" $User</li>"
	if [ ! -z "$UserPath" ]
	then
		if [[ "$SERVER" = $(hostname-fqdn) ]]
		then
			[ ! -d "$UserPath/$COPYDIR" ] && mkdir "$UserPath/$COPYDIR"
			cp -f -v -R "$SOURCEDIR"/* "$UserPath/$COPYDIR"
			chown -v -R "$User" "$UserPath/$COPYDIR"
			chmod -R 0600 "$UserPath/$COPYDIR"
			chmod -R u+X "$UserPath/$COPYDIR"
		else
			ssh -o PasswordAuthentication=no -o ConnectTimeout=3 "$SERVER" '
			[ ! -d "'"$UserPath/$COPYDIR"'" ] && mkdir "'"$UserPath/$COPYDIR"'"		
			'
			scp -v "$SOURCEDIR"/* root@"$SERVER:$UserPath/$COPYDIR"
			ssh -o PasswordAuthentication=no -o ConnectTimeout=3 "$SERVER" '
			chown -v -R "'"$User"'" "'"$UserPath/$COPYDIR"'"
			chmod -R 0600 "'"$UserPath/$COPYDIR"'"
			chmod -R u+X "'"$UserPath/$COPYDIR"'"
			'
		fi
	fi	
done
echo "</ul>"

#Delete uploaded files
rm -f -R /var/www/karoshi/web_upload
exit

